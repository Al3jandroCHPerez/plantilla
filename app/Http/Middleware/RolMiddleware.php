<?php

namespace App\Http\Middleware;

use Closure;


class RolMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
          $roles = array_slice(func_get_args(), 2);
        if (auth()->check() && auth()->user()->isAdmin())
         return $next($request);
           
        foreach($roles as $role) {
        // Check if user has the role This check will depend on how your roles are set up
            if( auth()->user()->hasRole($role)){
             return $next($request);
           }  
         }
        
        
     return redirect('/home');
    }
}
