<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MenuModel;
use App\Models\RolMenuModel;

class MenuController extends Controller{
    //
    public function index(){
    	$lista = MenuModel::where('status','=',1)->get();
    	return view('rolesmenu/menu',compact('lista'));
    }

    public function nuevomenu(Request $request){
    	$data = MenuModel::latest('id')->first();
    	$prioridad = $data->id ;
    	$prioridad ++;
    	$registro = new MenuModel();
    	$registro->etiqueta = $request->etiqueta; 
    	$registro->url = $request->url;
    	$registro->clase = 'kt-menu__link-icon '.$request->icono;
    	$registro->nombre_menu = 'Framedev';
    	$registro->padre = 0;
    	$registro->status = 1 ;	
		$registro->prioridad = $prioridad;
    	$registro->save();
        //le da acceso al  menu creado  al rol root
        $permiso = new RolMenuModel();
        $permiso->id_rol = 1;
        $permiso->id_menu = $registro->id;
        $permiso->status = 1;
        $permiso->save();

    	return redirect()->route('menu');
    }

    public function eliminamenu(Request $request){
        $registro  = MenuModel::find($request->id);
        $registro->status = 0;
        $registro->save();

        $permiso = RolMenuModel::where('id_menu','=',$request->id)->get();
        foreach ($permiso as $per) {
            $per->status = 0;
            $per->save();
        }

        return redirect()->route('menu');
    }
 
}
