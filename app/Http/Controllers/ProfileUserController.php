<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;


class ProfileUserController extends Controller{
    //
    public function perfil(){
  		$id = Auth::user()->id;
  		    $user = User::from('users')
  		     	->select('users.*')
  		        ->where('users.id', '=', $id)->get();
  		return view('users/perfil')->with('user',$user);
  	}

  	public function updatePassWord(Request $request){
      $id = Auth::user()->id;
      $usuarios = User::find($id);
        if((Hash::check($request->get('passwordAct'),Auth::user()->password))){
             $passwordnew = $request->passwordnew;
             $passwordneww = $request->passwordneww;
                  $usuarios->password=bcrypt($passwordnew);
                  $usuarios->save();  
        }else{
          return 0;
        }
    }

    public function updateImage(Request $request){

        $this->validate($request, [
        	'avatar' => 'required|image'
    	  ]);

        $file = $request->file('avatar');
        $id = Auth::user()->id;
        $usuario = User::find($id);
        $usuario->avatar = $request->file('avatar')->store('imagenes');
        $saved =$usuario->save(); 
        $data['success'] = $saved;
        $data['path'] = $usuario->getAvatarUrl();


     	return $data;
    }
}
