<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\EstablecimientoSalud;
use Illuminate\Http\Request;
use App\Models\CatEntidad;
use App\Models\RolModel;
use App\Models\RolUserModel;
use Response,DB,Config;
use App\Models\User;

class UserControlController extends Controller{
	public $successStatus = 200;
	
    public function index(){
		$usuarios = User::from('users')->select('*')->where('users.status',  '!=' , 0)->get();
		$catEntidad = CatEntidad::all();
		$establecimientoSalud = EstablecimientoSalud::all();
		$rol = RolModel::all();
    	return view('users/control_personal', compact('usuarios', 'catEntidad', 'establecimientoSalud', 'rol'));
	}
	
	public function getUsers(){
		$usuarios = User::with(
			'roles_user.roles',
			'entidad',
			'unidad',
			'unidad.institucion',
			'unidad.tipologia'
		)->get();
		return $usuarios;
	}

    public function registerUser(Request $request){
		// encriptamos el password antes de insertar en base
		$request->merge([
			'password' => bcrypt($request->password),
		]);
		$addUser = User::create($request->except('id_rol'));
		$addRolUser = RolUserModel::create([
			'id_user' => $addUser->id,
			'id_rol' => $request->id_rol
		]);
		return redirect()->back()->with('success', 'El usuario se registro correctamente &#128521;');
    }

    public function updateData(Request $request){
		
		// actualizamos la informacion del usuario
    	$user = User::find($request->id); 
    	$user->nombre = $request->nombre;
		$user->puesto = $request->puesto;
		$user->email = $request->email;
		$user->id_entidad = $request->id_entidad;
		$user->subdireccion = $request->subdireccion;
		$user->telefono = $request->telefono;
		$user->red = $request->red;
		$user->id_unidad = $request->id_unidad;
		$user->save();
		// actalizamos el rol del usuario
		// $user->rol->id_rol = $request->id_rol;
		// $user->rol->save();
		$response = response()->json(['status'=>true,'data'=> $user],  $this->successStatus);
		return $response;
    	
    }

    public function deleteData(Request $request){
    	//cambia estatus del usuario a 0
    	$id = Auth::user()->id;
    	$usuarios = User::find($request->id);
    	$usuarios->status = 0;
    	$usuarios->save();
    }

    public function updatePassword(Request $request){
		//cambia el password desde la vista controlPersonal
      	$usuarios = User::find($request->user_id);     
        $newPassword = $request->newPassword;
        $reNewPassword = $request->reNewPassword;
        if($newPassword == $reNewPassword){
            $usuarios->password=bcrypt($newPassword);
            $usuarios->save();  
		} 
		$response = response()->json(['status'=> true],  $this->successStatus);
		return $response;
	}
	
	public function getUser($id_user){
		$usuario = User::with(
			'rol.roles',
			'entidad',
			'unidad'
		)->find($id_user); 
		$response = response()->json(['data'=> $usuario],  $this->successStatus);
		return $response;
	}

	public function addRolUser(Request $request){
		$search = RolUserModel::where([
			['id_user', $request->id_user],
			['id_rol', $request->id_rol]
		])->exists();
		if ($search) {
			$data = [
				'status' => false,
				'message' => 'El usuario ya cuenta con ese rol'
			];
		}else{
			$addRol = RolUserModel::create([
				'id_user' => $request->id_user,
				'id_rol' => $request->id_rol,
				'status' => 1
			]);
			$data = [
				'status' => true,
				'message' => 'Se agrego el rol al usuario'
			];
		}
		$response = response()->json(['data'=> $data],  $this->successStatus);
		return $response;
	}

	public function getRolUser(Request $request, $id_user){
		$usuario = User::with('roles_user.roles')->find($id_user); 
		$response = response()->json(['data'=> $usuario],  $this->successStatus);
		return $response;
	}

	public function deleteRolUser($id_user, $id_rol){
		$deleteRol = RolUserModel::where([
			['id_user', $id_user],
			['id_rol', $id_rol]
		])->delete();
		if ($deleteRol == 1) {
			$response = response()->json([
				'status' => true,
				'message' => 'El rol se elimino correctamente.'
			],  $this->successStatus);
		}else{
			$response = response()->json([
				'status' => true,
				'message' => 'El rol no existe en este usuario.'
			],  $this->successStatus);
		}
		
		return $response;
		
	}

	public function deleteUser($id_user){
		// seleccionamos el usuario
		$user = User::find($id_user);
		// Eliminamos el usaurio
		$user->delete(); 
		// Eliminamos sus roles con ayuda de la relación
		$user->roles_user()->delete();
		if ($user == true) {
			$response = response()->json([
				'status' => true,
				'message' => 'El usuario se elimino correctamente.'
			],  $this->successStatus);
		}else{
			$response = response()->json([
				'status' => true,
				'message' => 'El usuario no existe en este usuario.'
			],  $this->successStatus);
		}

		return $response;
	}
}
