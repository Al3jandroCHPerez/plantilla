<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RolMenuModel;
use App\Models\RolModel;
use App\Models\MenuModel;

class RolController extends Controller{
    
    //
    public function index(){
    	$lista = RolModel::where('status','=',1)->get();
        $menu = MenuModel::all();
    	return view('rolesmenu/roles',compact('lista','menu'));
    }

    public function nuevorol(Request $request){
    	$registro = new RolModel();
    	$registro->rol = $request->rol; 
    	$registro->nombre_rol = $request->nombre_rol;
    	$registro->descripcion = $request->descripcion_rol;
    	$registro->status = 1 ;
    	$registro->save();
        //le da acceso basico al perfil al rol creado
        $permiso = new RolMenuModel();
        $permiso->id_rol = $registro->id;
        $permiso->id_menu = 1;
        $permiso->status = 1;
        $permiso->save();

    	return redirect()->route('roles');
    }

    public function eliminarol(Request $request){
        $registro  = RolModel::find($request->id);
        $registro->status = 0;
        $registro->save();

        $permiso = RolMenuModel::where('id_rol','=',$request->id)->get();
        foreach ($permiso as $per) {
            $per->status = 0;
            $per->save();
        }
    
        return redirect()->route('roles');
    }

    public function permisos(Request $request){
        foreach($request->id_menu as $menu){
            $busca = RolMenuModel::where('status','=',1)->where('id_menu','=',$menu)->where('id_rol','=',$request->id_rol)->get();
            if(empty($busca[0])){
                //solo guarda si no existe en esta base de datos 
                $registro = new RolMenuModel();
                $registro->id_rol = $request->id_rol;
                $registro->id_menu = $menu;
                $registro->status = 1; 
                $registro->save();
            }
        }
        
        return redirect()->route('roles');
    }
}
