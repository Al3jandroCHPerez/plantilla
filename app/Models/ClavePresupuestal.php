<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClavePresupuestal extends Model
{
    //
    protected $table = 'clave_presupuestal';

    public function getClave($clues,$id_entidad){
     	$buscaClave = ClavePresupuestal::where('clues','=',$clues)->where('id_entidad','=',$id_entidad)->get();
    	if(empty($buscaClave[0]))
            {
        		return 0;
        	}
    	else{
    		return $buscaClave[0]->id;
    	}
     }
}
