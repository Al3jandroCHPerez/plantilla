<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MenuModel;

class RolMenuModel extends Model
{
    
    protected $table ='rol_menu';

    protected $fillable = [ 'id_rol' ,'id_menu','status'];

    public function roles_menu(){
        return $this->hasMany(MenuModel::class,'id_menu');
    }

}
