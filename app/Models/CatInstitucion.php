<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatInstitucion extends CatMaster
{
    //
    public function getInstitucionId($cve){
        
        $id='false';	
        if ($this->catInstitucion()->institucion($cve)->count() == 1 ){
			$id = $this->catInstitucion()->institucion($cve)->first()->id;	
		}
      	return $id;
    }

     public function scopeCatInstitucion($query){
		return $query->where('catalogo', 'cat_institucion');
	}     


     public function scopeInstitucion($query, $cve_institucion){
		return $query->where('cve', $cve_institucion);
	} 
}
