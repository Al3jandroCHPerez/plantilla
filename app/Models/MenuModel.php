<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RolMenuModel;
use App\Models\RolModel;

class MenuModel extends Model{
  
    protected $table ='menu';
    protected $fillable = [ 'etiqueta', 'nombre_menu','prioridad','padre','url','clase','status'];

    public function roles(){
        return $this->belongsTo(RolMenuModel::class,'id_rol','id');
    }


    public function opcionesMenu($id_rol){
      $menuss = $this::from('menu')
          ->join('rol_menu', 'menu.id', '=', 'rol_menu.id_menu')
          ->orderby('padre')
          ->orderby('prioridad')  
          ->orderby('etiqueta')         
          ->where('rol_menu.id_rol','=', $id_rol)
          ->where('menu.status','=', '1')
          ->select('menu.*')
          ->get()
          ->toArray();

         return $menuss;
    }

	  public function gethijos($data, $line){
      $hijos = [];
      foreach ($data as $line1) {
        if ($line['id'] == $line1['padre']) {
          $hijos = array_merge($hijos, [ array_merge($line1, ['submenu' => $this->gethijos($data, $line1) ]) ]);       
        }
      }
      return $hijos;
    }
}
