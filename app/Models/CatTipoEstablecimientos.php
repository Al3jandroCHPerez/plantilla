<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CatMaster;

class CatTipoEstablecimientos extends CatMaster
{
    //
    public function getTipoEstablecimientoId($cve){
        
        $id='false';	
        if ($this->catTipoEstablecimiento()->tipoEstablecimiento($cve)->count() == 1 ){
			$id = $this->catTipoEstablecimiento()->tipoEstablecimiento($cve)->first()->id;	
		}
      	return $id;
    }

     public function scopeCatTipoEstablecimiento($query){
		return $query->where('catalogo', 'cat_establecimiento');
	}     


     public function scopeTipoEstablecimiento($query, $cve){
		return $query->where('cve', $cve);
	}  
}
