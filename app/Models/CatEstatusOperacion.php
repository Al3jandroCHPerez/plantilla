<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CatMaster;

class CatEstatusOperacion extends CatMaster
{
    //
     public function getEstatusOperacionId($cve){
        
        $id='false';	
        if ($this->catEstatusOperacion()->estatusOperacion($cve)->count() == 1 ){
			$id = $this->catEstatusOperacion()->estatusOperacion($cve)->first()->id;	
		}
      	return $id;
    }

     public function scopeCatEstatusOperacion($query){
		return $query->where('catalogo', 'cat_estatus_operacion');
	}     


     public function scopeEstatusOperacion($query, $cve){
		return $query->where('cve', $cve);

	}
}
