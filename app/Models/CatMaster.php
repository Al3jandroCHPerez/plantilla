<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatMaster extends Model
{
    //
    protected $table ='cat_master';
    
    protected $fillable = ['parent_id', 'catalogo', 'etiqueta', 'cve', 'activo', 'orden', 'valor' ];
}
