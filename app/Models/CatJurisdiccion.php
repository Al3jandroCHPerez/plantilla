<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatJurisdiccion extends Model
{
    //
    protected $table ='cat_jurisdiccion';
    
    protected $fillable = ['jurisdiccion', 'cve_jurisdiccion'];

    public function getJurisdiccionId($jurisdiccion, $cve_jurisdiccion){
		
		$jurisdiccion= $this->where('cve_jurisdiccion', $cve_jurisdiccion)->where('jurisdiccion', $jurisdiccion);
		if ($jurisdiccion->count() == 1 )
			{
				$id = $jurisdiccion->first()->id;	
			}
		else{
			    $id='false';	
		}
			    return $id;
	}
}
