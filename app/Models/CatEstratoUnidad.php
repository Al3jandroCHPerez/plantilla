<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatEstratoUnidad extends CatMaster
{
    //
    public function getEstratoUnidadId($cve){
        $id='false';  
        if ($this->catEstratoUnidad()->estratoUnidad($cve)->count() == 1 ){
          $id = $this->catEstratoUnidad()->estratoUnidad($cve)->first()->id;  
        }
        return $id;
    }

    public function scopeCatEstratoUnidad($query){
      return $query->where('catalogo', 'cat_estrato_unidad');
    }     

    public function scopeEstratoUnidad($query, $cve){
      return $query->where('cve', $cve);
    }
}
