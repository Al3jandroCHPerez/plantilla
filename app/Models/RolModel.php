<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RolModel extends Model
{
    protected $table ='rol';

    protected $fillable = [ 'rol','nombre_rol','descripcion','status'];

    protected $dates = ['created_at', 'updated_at'];


    public function roles_user(){ 
        return $this->hasMany(RolUserModel::class,'id_rol');
    }
}
