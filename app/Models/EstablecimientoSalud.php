<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CatNivelAtencion;

class EstablecimientoSalud extends Model
{
    //
    protected $table ='establecimientos_salud';
    
    protected $fillable = ['municipio_id','clues', 'entidad_id', 'localidad_id', 'jurisdiccion_id', 'institucion_id', 'tipologia_id', 'consultoriosmed_gral', 'consultoriosareas', 'total_de_consultorios', 'casmas_hosp', 'camas_areas', 'nombre_unidad', 'vialidad', 'numero_exterior', 'numero_interior', 'observaciones_de_la_direccion', 'asentamiento', 'codigo_postal', 'id_status', 'rfc_del_establecimiento', 'fecha_de_inicio_de_operacion', 'longitud', 'latitud',  'nivelatencion_id' , 'estratounidad_id', 'lat', 'lon'
    ];

    public function nivelAtencion(){ //belong es pertenece
      	return $this->belongsTo(CatNivelAtencion::class,'nivelatencion_id');
   }

    public function nombre_completo(){
        return $this->institucion->valor.' '.$this->tipologia->etiqueta.' '.$this->nombre_unidad;
    }
    public function tipologia(){ //belong es pertenece
        return $this->belongsTo(CatTipologia::class,'tipologia_id');
    }
    public function institucion(){ //belong es pertenece
        return $this->belongsTo(CatInstitucion::class,'institucion_id');
    }
   
}
