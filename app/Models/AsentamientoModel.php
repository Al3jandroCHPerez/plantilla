<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AsentamientoModel extends Model
{
    //
    protected $table ='asentamiento';
  	public $timestamps = false;

}
