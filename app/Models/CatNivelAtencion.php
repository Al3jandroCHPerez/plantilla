<?php

namespace App\Models;

use App\Models\CatMaster;

use Illuminate\Database\Eloquent\Model;

class CatNivelAtencion  extends CatMaster
{
    //
    public function getNivelAtencionId($cve){  
      $id='false';  
      if ($this->catNivelAtencion()->nivelAtencion($cve)->count() == 1 ){
        $id = $this->catNivelAtencion()->nivelAtencion($cve)->first()->id;  
      }
      return $id;
    }

    public function scopeCatNivelAtencion($query){
      return $query->where('catalogo', 'cat_nivel_atencion');
    }     

    public function scopeNivelAtencion($query, $cve){
      return $query->where('cve', $cve);
    }
}
