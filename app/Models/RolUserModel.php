<?php

namespace App\Models;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\RolModel;

use Illuminate\Database\Eloquent\Model;

class RolUserModel extends Model
{
    protected $table ='rol_user';

    protected $fillable = [
        'id_user',
        'id_rol'
    ];

    public function user(){
        return $this->belongsTo(User::class,'id_user','id');
    }
    public function roles(){
        return $this->belongsTo(RolModel::class,'id_rol','id');
    }
}
