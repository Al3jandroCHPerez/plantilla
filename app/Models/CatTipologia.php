<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CatMaster;

class CatTipologia extends CatMaster
{
    //
    public function getTipologiaId($valor, $cve){
        $id='99999';	
        if ($this->catTipologia()->tipologia($valor)->claveTipologia($cve)->count() == 1 ){
			$id = $this->catTipologia()->tipologia($valor)->claveTipologia($cve)->first()->id;	
		}
      	return $id;
    }

    public function scopeCatTipologia($query){
		return $query->where('catalogo', 'tipoUnidad');
	}     

    public function scopeTipologia($query, $valor){
        return $query->where('valor', $valor);
    }

    public function scopeClaveTipologia($query, $cve){
		return $query->where('cve', $cve);
    }
}
