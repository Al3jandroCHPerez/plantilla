<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Models\MenuModel;
use App\Models\RolModel;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "nombre",
        "puesto",
        "email",
        "id_entidad",
        "subdireccion",
        "telefono",
        "red",
        "id_unidad",
        "password",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getAvatarUrl()//funcion para llamar la imagen de cada usuario
    {
        $id = Auth::user()->id;
        $user = User::from('users')
            ->select('users.avatar')
            ->where('users.id', '=', $id)->get();
           //dd(Storage::url($user[0]->avatar));
        return asset(Storage::url($user[0]->avatar));  
    }



    public function menuPrincipal(){//funcion para pintar el menu principal
        
        //$rol = Auth::user()->id;
        $roles = $this->roles_user;
        $menus = new MenuModel();
        $menuAll = [];
        foreach ($roles as $rol) {
            
             $data = $menus->opcionesMenu($rol->id_rol);
             
             
             foreach ($data as $line) {
                 $item = [ array_merge($line, ['submenu' => $menus->gethijos($data, $line) ]) ];
                 $menuAll = array_merge($menuAll, $item);
            }
               
             
        }
        return $menus->menuAll = $menuAll;
    }

    public function menuPrincipal2(){
        // Consulto al menu padre e hijo
        $menusPadres = MenuModel::where('padre', '0')->get();
        $subMenus = MenuModel::where('padre', '!=','0')->get();
        $rolUser = $this->roles_user;
        $rolMenu = RolMenuModel::all();
        // Declaro arreglo para el menu
        $fullMenu = [];
        // recorreo el menu padre
        $index = 0;
        foreach($menusPadres as $menu){
                // inserto el menu en el arreglo
                $fullMenu[$index] = $menu;
                // recorro el submenu
                $index2 = 0;
                /**
                 * Se declara arreglo de submenu, se declara aquí
                 * Para que al entrar a otro submenu no repita valores
                 */
                $fullSubMenu = [];
                foreach($subMenus as $submenu){
                    // Si el submenu pertenece al menu padre creo su arreglo
                    if ($menu->id == $submenu->padre) {
                        $fullSubMenu[$index2] = $submenu;
                        $fullMenu[$index]['submenu'] = $fullSubMenu;
                    }
                    $index2++;
                }
                $index++;
        }
        return $fullMenu;
    }
    public function validaRolMenuUsuario($menu){
        $rolUser = $this->roles_user;
        $exists  = [];
        $index = 0;
        foreach($rolUser as $rol){
            $rolMenu = RolMenuModel::where([
                ['id_rol', $rol->id_rol],
                ['id_menu', $menu->id]
            ])->exists();
            $exists[$index] = $rolMenu;
            $index++;
        }
        if (in_array(true, $exists)) {
            return 1;
        }else{
            return 0;
        }
    }

    public function isAdmin(){
        $roles = $this->roles_user;
        $bandera = array();
        foreach($roles as $rol) {
            if($rol->roles->rol == 'root'){
                $ban =1;
                array_push($bandera, $ban);
            }elseif($rol->roles->rol != 'root'){
                $ban=2;
                array_push($bandera, $ban);
            }
        }
        if(in_array(1, $bandera)){
            return true;
        }else{
            return false;
        }
    }

    public function hasRole($role){
        $roles = $this->roles_user;
        $bandera = array();
        foreach($roles as $rol) {
            if($rol->roles->rol == $role){
                $ban =1;
                array_push($bandera, $ban);
            }elseif($rol->roles->rol != $role)
                $ban=2;
                array_push($bandera, $ban);
        }
        if(in_array(1, $bandera)){
            return true;
        }else{
            return false;
        }
    }


    public function roles_user(){ 
        return $this->hasMany(RolUserModel::class,'id_user');
    }

    public function entidad(){
        return $this->belongsTo(CatEntidad::class, 'id_entidad', 'id_entidad');
    }

    public function unidad(){
        return $this->belongsTo(EstablecimientoSalud::class, 'id_unidad', 'id');
    }
    public function rol(){
        return $this->belongsTo(RolUserModel::class, 'id', 'id_user');
    }
}
