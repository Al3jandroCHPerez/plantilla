<p align="center"><img src="./ImageReadme/ISSSTE_logo.png" height="130" width="400"></p>
 

 
## Configuración inicial
 
Después de clonar el proyecto se debe realizar las siguientes configuraciones.
 
### Instalaciones requeridas
 
Instalamos lo requerido con composer, para esto en la terminal ejecutamos:
 
`composer i`
 
Instalamos los paquetes requeridos de node:
 
`npm i`

Para crear el enlace simbólico:

`php artisan storage:link`

 
### Crear archivo .env
 
En la terminal hay que ejecutar el siguiente comando:
 
`cp .env.example .env`
 
### Crear key requerida en laravel
 
Creamos la APP key que require laravel en su archivo `.env`
 
`php artisan key:generate`
 
### Crea tu base de datos en mysql
 
En tu mysql crea una base de datos llamada: `plantilla`:
 
En el archivo `.env` vamos a editar el apartado de conexión colocando los valores requeridos para tu conexion a MySql:
 
```env
DB_CONNECTION=mysql
DB_HOST=192.168.0.3
DB_PORT=3306
DB_DATABASE=plantilla
DB_USERNAME=root
DB_PASSWORD=12345678
```
 
En cuanto se edite esta información hay que ejecutar en terminal el comando:
 
`php artisan config:clear`
 
### Crear migraciones
 
Para terminar hay que ejecutar:
 
`php artisan migrate`

Ejecutamos los seeders iniciales.

`php artisan db:seed`

Uno de los seeders que se ejecuta arriba agregara a un usuario con los siguientes accesos:

* email: root@issste.gob.mx
* password: 12345678

Por ultimo hay que ejecutar:

´npm run dev´


## Para Crear Roles, Permisos y Menú

### Creamos un nuevo Menú

<p align="center"><img src="./ImageReadme/menuyroles/1.png"></p>

Para poder agregar un icono a un menú se requiere seguir el link copiar el texto del icono y agregar en el cuestionario.

<p align="center"><img src="./ImageReadme/menuyroles/2.png"></p>

Una vez guardado el menú se visualiza en la lista y en el menú

<p align="center"><img src="./ImageReadme/menuyroles/3.png"></p>

<p align="center"><img src="./ImageReadme/menuyroles/4.png"></p>

El menú que se agrega es automáticamente visto por el rol ROOT , 
Para eliminar un menú basta con dar click en el botón rojo que tiene cada menú en la lista. 

<p align="center"><img src="./ImageReadme/menuyroles/5.png"></p>

### Creamos un nuevo Rol

<p align="center"><img src="./ImageReadme/menuyroles/6.png"></p>

El campo Rol es necesario llenar con una abreviación del nombre del rol

<p align="center"><img src="./ImageReadme/menuyroles/7.png"></p>

Cada Rol creado tiene precargado la vista a Mi Perfil, 
Una vez guardado el Rol puede ser visto en la lista de roles:

<p align="center"><img src="./ImageReadme/menuyroles/8.png"></p>

Para eliminar un menú basta con dar click en el botón rojo que tiene cada rol en la lista 

<p align="center"><img src="./ImageReadme/menuyroles/9.png"></p>

### Damos Permisos de Menú a un Rol

<p align="center"><img src="./ImageReadme/menuyroles/10.png"></p>

<p align="center"><img src="./ImageReadme/menuyroles/11.png"></p>

Al dar click en el botón verde de cada Rol:

<p align="center"><img src="./ImageReadme/menuyroles/12.png"></p>

Podremos seleccionar los permisos que le se le asignaran a cada rol con respecto a cada menú

### Querys para Agregar un Sub Menú

Creamos el Menú base:

<p align="center"><img src="./ImageReadme/menuyroles/query1.png"></p>

``` SQL

INSERT INTO `plantilla`.`menu`
(`id`,`etiqueta`,`nombre_menu`,`prioridad`,`padre`,`url`,`clase`,`created_at`,`updated_at`)
VALUES ('7','Sub Menu','Framedev','7','0',' ','kt_menu__link-icon','2020-08-17 15:08:18','2020-08-17 15:08:18');
SELECT `id`,`etiqueta`,`nombre_menu`,`prioridad`,`padre`,`url`,`clase`,`created_at`,`updated_at`
FROM `plantilla`.`menu`
WHERE `id` = 7

```

Creamos dos submenus:

<p align="center"><img src="./ImageReadme/menuyroles/query2.png"></p>

``` SQL

INSERT INTO `plantilla`.`menu`
(`id`,`etiqueta`,`nombre_menu`,`prioridad`,`padre`,`url`,`clase`,`created_at`,`updated_at`)
VALUES ('8','Sub1','Framedev','8','7','/sub2','kt_menu__link-bullet kt-menu__link-bullet--dot','2020-08-17 15:08:18','2020-08-17 15:08:18');
SELECT `id`,`etiqueta`,`nombre_menu`,`prioridad`,`padre`,`url`,`clase`,`created_at`,`updated_at`
FROM `plantilla`.`menu`
WHERE `id` = 8


```

<p align="center"><img src="./ImageReadme/menuyroles/query3.png"></p>

``` SQL

INSERT INTO `plantilla`.`menu`
(`id`,`etiqueta`,`nombre_menu`,`prioridad`,`padre`,`url`,`clase`,`created_at`,`updated_at`)
VALUES ('9','Sub2','Framedev','9','7','/sub2','kt_menu__link-bullet kt-menu__link-bullet--dot','2020-08-17 15:08:18','2020-08-17 15:08:18');
SELECT `id`,`etiqueta`,`nombre_menu`,`prioridad`,`padre`,`url`,`clase`,`created_at`,`updated_at`
FROM `plantilla`.`menu`
WHERE `id` = 9


```

Este es el resultado en Base de Datos:

<p align="center"><img src="./ImageReadme/menuyroles/query4.png"></p>

Asignamos permisos:

<p align="center"><img src="./ImageReadme/menuyroles/query5.png"></p>

``` SQL

INSERT INTO `plantilla`.`rol_menu` (`id`,`id_rol`,`id_menu`)
VALUES('6','1','7')
SELECT `id`,`id_rol`,`id_menu`, `created_at`,`updated_at`
FROM `plantilla`.`rol_menu` WHERE `id`=6


```

``` SQL

INSERT INTO `plantilla`.`rol_menu` (`id`,`id_rol`,`id_menu`)
VALUES('7','1','8')
SELECT `id`,`id_rol`,`id_menu`, `created_at`,`updated_at`
FROM `plantilla`.`rol_menu` WHERE `id`=7


```

``` SQL

INSERT INTO `plantilla`.`rol_menu` (`id`,`id_rol`,`id_menu`)
VALUES('8','1','9')
SELECT `id`,`id_rol`,`id_menu`, `created_at`,`updated_at`
FROM `plantilla`.`rol_menu` WHERE `id`=8


```


Este es el resultado en Base de Datos:

<p align="center"><img src="./ImageReadme/menuyroles/query6.png"></p>



