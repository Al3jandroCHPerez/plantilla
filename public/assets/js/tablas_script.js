 //funcion que se encarga de visualizar todaas las entidades
 $(document).ready( function() 
   {
      $('#inventario_table').DataTable({ //tabla para cargar todas las entidades
          "pageLength": 5,
           processing: true,
           serverSide: true,
           ajax: "censo/entidad",
           columns: [
                    { data: 'id_entidad', name: 'id_entidad' },
                    { data: 'entidad', name: 'entidad' },
                    { render: function () {

                        return '<button  class="btn btn-info btn-outline-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" data-toggle="modal" data-target="#editt" ><i class="fas fa-cog left"></i></button>';}
                    } 
           ]
        });
   } );
$('#table_unidades').DataTable();
//tablas pagina principal de referencias
$('#table_ref1').DataTable({
	"order": [[0, "desc"]]
});
$('#table_ref2').DataTable();
$('#table_ref3').DataTable();
$('#table_ref4').DataTable();
$('#table_ref5').DataTable({
  "order": [[0, "desc"]]
});
//temina referencias
//tablas modulo medicocovid **imagen**
$('#table_imagen1').DataTable();
$('#table_imagen2').DataTable();
$('#table_imagen3').DataTable();
// termina modulo medicocovid **imagen**
//tablas modulo medicocovid **diagnostico**
$('#pacientesCovidTable').DataTable();
// termina modulo medicocovid **diagnostico**

   $('#table_reporte').DataTable({
     "order": [[0, "desc"]]
   }
   );

   $('#table_entidad').DataTable({
        "lengthMenu": [[32, 50, 75, -1], [32, 50, 75, "All"]]
    });
   $('#table_unidadesMed').DataTable({
        "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "All"]]
    });
   $('#table_unidadesMed2').DataTable({
        "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "All"]]
    });
    
    $('#table_seguimientos_all').DataTable({
      "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "All"]]
  });

//funcion para el listado de fuaps por unidad 
$('#filtroFuap').click(function(){
  var id_ubicacion =  $('#unidades').val();
  $('#inventario_table').DataTable({
    processing: true,
    serverSide: true,//sirve para traer los datos del servidor
    // destroy: true,
    ajax:{
      url : '/censo/recusosfisicos',
      // type: "get",
      // data: {"id_ubicacion": id_ubicacion}
    },
    columns: [
      { data: 'Id_RecFisico', name: 'Id_RecFisico' },
      { data: 'id_Fisicos', name: 'id_Fisicos'},
      { data: 'id_Subrubro', name: 'id_Subrubro' },
      { data: 'bFueraServicio', name: 'bFueraServicio' },
      { data: 'Id_CausaFueraServ', name: 'Id_CausaFueraServ' },
    ]
  });
});
//-- Tabla de vista Usuarios --//
var table = $('#user_table').DataTable();
table.on( 'draw', function () {
    $('.btn-edit').click(function(){          //-- Funcion para pintar el select del editar --//
        var idestado = $(this).data("idestado")
        var idrol = $(this).data("idrol")
        var ulaboral = $(this).data("ulaboral")
        var selectEstado = $('#estadosedit')
        // console.log(idestado)
        // console.log(idrol)
        // console.log(ulaboral)
        $.ajax({                           //-- ajax de editar estado
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: '/user/selects_entidad',
          dataType: 'html',
          success: function(data){
            var valores = JSON.parse(data);
            var estadosedit = "#estadosedit"
            $.each(valores.entidad,function(key, registro) {
              var cadena = registro.entidad.toUpperCase();
              if(idestado == registro.id_entidad){
                $(estadosedit).append('<option selected value='+registro.id_entidad+'>'+ cadena +'</option>');
              }else{
                $(estadosedit).append('<option value='+registro.id_entidad+'>'+ cadena +'</option>');
              } //fin del else  
            });//fin del each 
          },
        }) 

        $.ajax({                               //-- ajax de editar rol
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: '/user/selects_rol',
          dataType: 'html',
          success: function(data){
            var valores2 = JSON.parse(data);
            var roledit = "#edit_rol_usuario"
    
            $.each(valores2.rol,function(key, registro2) {
              var cadena2 = registro2.nombre_rol.toUpperCase();
              if(idrol == registro2.id){
                $(roledit).append('<option selected value='+registro2.id+'>'+ cadena2 +'</option>');
              }else{
                $(roledit).append('<option value='+registro2.id+'>'+ cadena2 +'</option>');
              } //fin del else  
            });//fin del each 
          },// fin del success
        })//fin de ajax 

        // Listamo las unidades iniciales 🔥🔥🔥
        listarUnidadLaboral(idestado, ulaboral)

        // Se ejecuta cunado el select cambia de valor
        selectEstado.change( function() {
          listarUnidadLaboral(selectEstado.val(), ulaboral)
        });
    }) //-- fin de la funcion btn edit --//
});

function listarUnidadLaboral(idestado, ulaboral){
  $.ajax({        /// select de unidad laboral
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type: 'post',  
    url: '/user/selects_unidadLaboralEdit',
    dataType: 'html',
    data: { 
      '_token':$('input[name=_token').val(),
      'estado':idestado
    }, 
    success: function(data){
      var ulaboedit = "#unidad_laboral_edit"
      var $area_selector = $('#unidad_laboral_edit');  
      var valores3 = JSON.parse(data);
      console.log(valores3)

      $area_selector      //-- ESTA PARTE ES PARA LIMPAIR EL SELECT 
                  .find('option')
                  .remove()
                  .end()
                  .append('<option value="">Seleccione...</option>');

       $.each(valores3.unidad_laboralE,function(key, registro3) {
          var cadena3 = registro3.nombre_unidad.toUpperCase();
          if(ulaboral == registro3.nombre_unidad){
            $(ulaboedit).append('<option selected value='+registro3.id+'>'+ cadena3 +'</option>');
          }else{
            $(ulaboedit).append('<option value='+registro3.id+'>'+ cadena3 +'</option>');
          } //fin del else      
       });//fin del each 
    }, //fin de success
      error: function(resp_success){ alerta('Alerta!','Error Modal');}
  }) //fin de ajax

} //-- fin de la funcion --//

//-- Tabla de vista Ubicación --//
$('#ubicaciones_table').DataTable({
    processing: true,
    serverSide: true,
    ajax: "ubicacion/ubicacionesList",
    columns: [
      { data: 'id', name: 'id' },
      { data: 'zona.nombre_zona', name: 'zona.nombre_zona' },
      { data: 'inmueble', name: 'inmueble' },
      { data: 'codigo_postal', name: 'codigo_postal' },
      { data: 'municipio.municipio', name: 'municipio.municipio' },
      { data: 'colonia', name: 'colonia' },
      { data: 'ubicacion', name: 'ubicacion'}
    ]
});
//-- Fin fucnion vista ubicación  --//

