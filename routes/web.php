<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	//Perfil del Usuario
	Route::get('/perfil', 'ProfileUserController@perfil')->name('perfil')->middleware('role:root,asesor,jefes');
	Route::post('/updatePassWord','ProfileUserController@updatePassWord')->middleware('role:root');
	Route::post('/subir_imagen','ProfileUserController@updateImage')->middleware('role:root');
	//termina Perfil del Usuario
	//Control de Usuarios
    Route::get('/datos_personal', 'UserControlController@index')->middleware('role:root');
    Route::post('/updateData','UserControlController@updateData')->middleware('role:root');
    Route::post('/registroUsuario','UserControlController@registerUser')->middleware('role:root');
    Route::post('/deletePersonal','UserControlController@deleteData')->middleware('role:root');
	Route::post('/updateDataPassword','UserControlController@updatePassword')->middleware('role:root');
	//temina Control de Usuarios
	//Control de roles
	Route::get('/roles','RolController@index')->name('roles')->middleware('role:root');
	Route::post('/nuevorol','RolController@nuevorol')->middleware('role:root');
	Route::post('/eliminarol','RolController@eliminarol')->middleware('role:root');
	Route::post('/permisos','RolController@permisos')->middleware('role:root');
	//termina Control de Roles
	//Control de Menú
	Route::get('/menu','MenuController@index')->name('menu')->middleware('role:root');
	Route::post('/nuevomenu','MenuController@nuevomenu')->middleware('role:root');
	Route::post('/eliminamenu','MenuController@eliminamenu')->middleware('role:root');
	//termina Control de Menú

});


Route::fallback(function() {
    return abort(404);
});
