<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// peticiones para usuario
Route::get('/users', 'UserControlController@getUsers');
Route::get('/user/{id_user}', 'UserControlController@getUser');
Route::post('/addUser', 'UserControlController@addRolUser');
Route::get('/getroluser/{id_user}', 'UserControlController@getRolUser');
Route::delete('/deleteroluser/{id_user}/{id_rol}', 'UserControlController@deleteRolUser');
Route::delete('/deleteuser/{id_user}', 'UserControlController@deleteUser');