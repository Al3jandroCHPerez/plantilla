<?php

use Illuminate\Database\Seeder;
use App\Models\CatMaster;
use Keboola\Csv\CsvFile;

class CatEstratoUnidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $csv= new CsvFile(base_path().'/database/seeds/csv/cat_estrato_unidad.csv', ',');
        foreach($csv AS $row) {
    	  $estrato_unidad= new CatMaster;
          $estrato_unidad->create([
            'catalogo'=> 'cat_estrato_unidad', 
            'etiqueta'=>  $row[1],
       		'cve'=> $row[0],
			'valor'=> $row[1],
       	  ]);
        }
    }
}
