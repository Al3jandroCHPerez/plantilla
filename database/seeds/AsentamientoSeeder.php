<?php

use Illuminate\Database\Seeder;
use App\Models\AsentamientoModel;
use Keboola\Csv\CsvFile;

class AsentamientoSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        //
        $csv= new CsvFile(base_path().'/database/seeds/csv/aentamiento.csv', ';');
        foreach($csv AS $row) {
          $asentamiento = new AsentamientoModel;
          $asentamiento->create([
           'id_tipo_asentamiento'=> (int)$row[0],
           'asentamiento'=> $row[1],
           'codigo_postal'=> (int)$row[2],
           'cve_asentamiento'=> (int)$row[3],
           'tipo_zona'=> $row[4],
           'cve_ciudad'=> $row[5],
           'cve_municipio'=> $row[6],
           'id_entidad'=> $row[7]
          ]);
        }
    }
}
