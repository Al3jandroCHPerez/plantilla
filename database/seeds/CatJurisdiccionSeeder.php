<?php

use Illuminate\Database\Seeder;
use App\Models\CatJurisdiccion;
use Keboola\Csv\CsvFile;

class CatJurisdiccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $csv= new CsvFile(base_path().'/database/seeds/csv/jurisdiccion.csv', ',');
      	foreach($csv AS $row) {
    		$jurisdiccion= new CatJurisdiccion;
            $jurisdiccion->create([
            'cve_jurisdiccion'=> $row[0],    
       		'jurisdiccion'=> $row[1],
			
       	  ]);
        }
    }
}
