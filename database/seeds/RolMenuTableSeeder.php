<?php

use Illuminate\Database\Seeder;

class RolMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          ///////// rol root//////////////////////////
      DB::table('rol_menu')->insert(
        array(
        'id_rol'=>1,
        'id_menu'=>1,
        'created_at'=>'2019-07-02 03:50:18',
        'updated_at'=>'2019-07-02 03:50:18',
       ) );
 
       DB::table('rol_menu')->insert(
         array(
         'id_rol'=>1,
         'id_menu'=>2,
         'created_at'=>'2019-07-02 03:50:18',
         'updated_at'=>'2019-07-02 03:50:18',
        ) );
       
       DB::table('rol_menu')->insert(
         array(
         'id_rol'=>1,
         'id_menu'=>3,
         'created_at'=>'2019-07-02 03:50:18',
         'updated_at'=>'2019-07-02 03:50:18',
        ) );

       DB::table('rol_menu')->insert(
         array(
         'id_rol'=>1,
         'id_menu'=>4,
         'created_at'=>'2019-07-02 03:50:18',
         'updated_at'=>'2019-07-02 03:50:18',
        ) );

       DB::table('rol_menu')->insert(
         array(
         'id_rol'=>1,
         'id_menu'=>5,
         'created_at'=>'2019-07-02 03:50:18',
         'updated_at'=>'2019-07-02 03:50:18',
        ) );
 
    }
}
