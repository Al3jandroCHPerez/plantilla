<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(AddRolesTableSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(RolMenuTableSeeder::class);
        $this->call(AsentamientoSeeder::class);
        $this->call(CatJurisdiccionSeeder::class);
        $this->call(CatMunicipioSeeder::class);
        $this->call(CatEstatusOperacionSeeder::class);
        $this->call(CatEstratoUnidadSeeder::class);
        $this->call(CatEstablecimientosSeeder::class);
        $this->call(CatEntidadSeeder::class);
        $this->call(InstitucionesSeeder::class);
        $this->call(TipologiaUnidadSeeder::class);
        $this->call(LoadNivelAtencionSeeder::class);
        $this->call(clavePresupuestalseeder::class);
        $this->call(LoadClinicalLocationSeeder::class);

    }
}
