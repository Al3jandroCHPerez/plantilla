<?php

use Illuminate\Database\Seeder;
use Keboola\Csv\CsvFile;
use App\Models\ClavePresupuestal;  
use Illuminate\Support\Facades\Log;

class clavePresupuestalseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $csv= new CsvFile(base_path().'/database/seeds/csv/clave_presupuestal.csv',';');
     		foreach($csv AS $row) {

     		 	$clave = new ClavePresupuestal;
     		 	$clave->create([
     		 	  'clues' => $row[0],
     		 	  'clave_presupuestal' => $row[1],
     		 	  'id_red' => 1,
     		 	  'id_entidad' => $row[2]
     		 	]); 
     	}
    }
}
