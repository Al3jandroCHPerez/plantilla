<?php

use Illuminate\Database\Seeder;
use Keboola\Csv\CsvFile;
use App\Models\CatMaster;


class CatEstatusOperacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $csv= new CsvFile(base_path().'/database/seeds/csv/cat_estatus_operacion.csv', ',');
        foreach($csv AS $row) {
    		  $estatus_operacion= new CatMaster;
          $estatus_operacion->create([
       	    'catalogo'=> 'cat_estatus_operacion',
            'etiqueta'=> $row[1],
            'cve'=> $row[0],
			      'valor'=> $row[1],
       	  ]);
        }
    }
}
