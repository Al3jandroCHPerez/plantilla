<?php

use Illuminate\Database\Seeder;
use App\Models\CatMaster;
use Keboola\Csv\CsvFile;

class TipologiaUnidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $csv= new CsvFile(base_path().'/database/seeds/csv/tipoUnidad.csv', ',');
            $i=0;
            foreach($csv AS $row) {
          		$tipoUnidad= new Catmaster;
                $tipoUnidad->create([
             		'catalogo' => 'tipoUnidad',
             		'etiqueta'=> $row[1],
          			'cve'=> $row[0],
          			'activo' => 1,
          			'orden' => $i++,
          			'valor' => $row[1]
             	]);
        }

    }
}
