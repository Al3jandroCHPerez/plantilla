<?php

use Illuminate\Database\Seeder;
use App\Models\CatEntidad;
use App\Models\CatMunicipio;
use App\Models\CatJurisdiccion; 
use App\Models\CatMaster;
use App\Models\CatInstitucion;
use App\Models\CatTipoEstablecimientos;
use App\Models\CatTipologia;
use App\Models\CatEstratoUnidad;
use App\Models\EstablecimientoSalud;
use App\Models\CatEstatusOperacion;
use App\Models\CatNivelAtencion;
use App\Models\ClavePresupuestal;
use Keboola\Csv\CsvFile;

class LoadClinicalLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $entidad;
    protected $municipio;
    protected $jurisdiccion;
    protected $tipoEstablecimiento;
    protected $tipologia;
    protected $status;
    protected $nivelAtencion;
    protected $stratoUnidad;
    protected $institucion;
    protected $clavePresupuestal;


  public function __construct(CatEntidad $entidad, CatMunicipio $municipio,  CatJurisdiccion $jurisdiccion, CatTipoEstablecimientos $tipoEstablecimiento, CatTipologia $tipologia, CatEstatusOperacion $status, CatNivelAtencion $nivelAtencion, CatEstratoUnidad $stratoUnidad, CatInstitucion $institucion, clavePresupuestal $clavePresupuestal){
  		$this->entidad = $entidad;;
  		$this->municipio= $municipio;
  		$this->jurisdiccion=$jurisdiccion;
  		$this->tipoEstablecimiento=$tipoEstablecimiento;
  		$this->tipologia=$tipologia;
  		$this->status = $status;
  		$this->nivelAtencion=$nivelAtencion;
  		$this->stratoUnidad=$stratoUnidad;
    	$this->institucion = $institucion;
    	$this->clavepresupuestal = $clavePresupuestal;
    }

    public function run(){
     
     $csv= new CsvFile(base_path().'/database/seeds/csv/establecimientos.csv',',');
      foreach($csv AS $row) {
        //
        $establecimiento= new EstablecimientoSalud;

        $establecimiento->create([
          'clues'=> $row[0],
          'entidad_id' => $row[1],
          'municipio_id' => ($this->municipio->getMunicipioId((int)$row[3], (int)$row[1]) == 'false')? 0:$this->municipio->getMunicipioId((int)$row[3], (int)$row[1]),
          'jurisdiccion_id' => $this->jurisdiccion->getJurisdiccionId($row[6], (int)$row[7]),
          'institucion_id' => $this->institucion->getInstitucionId($row[10]),
          'tipo_establecimiento_id' => $this->tipoEstablecimiento->getTipoEstablecimientoId($row[12]),
          'tipologia_id' => $this->tipologia->getTipologiaId($row[13], $row[14]),
          'consultoriosmed_gral' => (int)$row[15],
          'consultoriosareas' => (int)$row[16],
          'camas_hosp' => (int)$row[17],
          'camas_areas' => (int)$row[18],
          'nombre_unidad'=> $row[19],
          'vialidad' => $row[20],
          'numero_exterior'  => $row[21],
          'numero_interior' => $row[22],
          'asentamiento' => $row[23],
          'observaciones_de_la_direccion' => $row[24],
          'codigo_postal' => $row[25],
          'status_id' => $this->status->getEstatusOperacionId($row[27]),
          'rfc_del_establecimiento' => ($row[28] == '')? NULL:$row[28],
          'id_clave_presupuestal'=> $this->clavepresupuestal->getClave($row[0],$row[1]),
          'nivelatencion_id' => $this->nivelAtencion->getNivelAtencionId($row[34]),
          'estratounidad_id' => $this->stratoUnidad->getEstratoUnidadId($row[36]),
          'lat' => $row[37] ,
          'lon' => $row[38] ,
       	]);	

      }
    }
}
