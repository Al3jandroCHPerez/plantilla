<?php

use Illuminate\Database\Seeder;

class AddRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rol')->insert(
            array(
            'rol'=>'root',
            'nombre_rol'=>'administrador',
            'descripcion'=>'rol con acceso a todo el sistema',  
            'created_at'=>'2020-04-10 03:50:18',
            'updated_at'=>'2020-04-10 03:50:18',
          ));
      
    }
}
