<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->insert(
            array(
              'id'=>1,
              'etiqueta'=>'Mi Perfil',
              'nombre_menu'=>'Framedev',
              'prioridad' => 1,
              'padre'=>0,
              'url'=>'/perfil',
              'clase'=>'kt-menu__link-icon flaticon2-user',
              'status'=>1,
              'created_at'=>'2019-07-02 03:50:18',
              'updated_at'=>'2019-07-02 03:50:18',
            ) 
          );
    
        DB::table('menu')->insert(
            array(
              'id'=>2,
              'etiqueta'=>'Control de Usuarios',
              'nombre_menu'=>'Framedev',
              'prioridad' => 2,
              'padre'=>0,
              'url'=>'/datos_personal',
              'clase'=>'kt-menu__link-icon flaticon-user-settings',
              'status'=>1,
              'created_at'=>'2019-07-02 03:50:18',
              'updated_at'=>'2019-07-02 03:50:18',
            ) 
          );

        DB::table('menu')->insert(
          array(
            'id'=>3,
            'etiqueta'=>'Roles y Menú',
            'nombre_menu'=>'Framedev',
            'prioridad' => 3,
            'padre'=>0,
            'url'=>'',
            'clase'=>'kt-menu__link-icon flaticon-customer',
            'status'=>1,
            'created_at'=>'2019-07-02 03:50:18',
            'updated_at'=>'2019-07-02 03:50:18',
          ) 
        );

        DB::table('menu')->insert(
          array(
            'id'=>4,
            'etiqueta'=>'Menú',
            'nombre_menu'=>'Framedev',
            'prioridad' => 4,
            'padre'=>3,
            'url'=>'/menu',
            'clase'=>'kt-menu__link-bullet kt-menu__link-bullet--dot',
            'status'=>1,
            'created_at'=>'2019-07-02 03:50:18',
            'updated_at'=>'2019-07-02 03:50:18',
          ) 
        );

        DB::table('menu')->insert(
          array(
            'id'=>5,
            'etiqueta'=>'Roles',
            'nombre_menu'=>'Framedev',
            'prioridad' => 5,
            'padre'=>3,
            'url'=>'/roles',
            'clase'=>'kt-menu__link-bullet kt-menu__link-bullet--dot',
            'status'=>1,
            'created_at'=>'2019-07-02 03:50:18',
            'updated_at'=>'2019-07-02 03:50:18',
          ) 
        );
    
    }
}
