<?php

use Illuminate\Database\Seeder;
use App\Models\CatMaster;
use Keboola\Csv\CsvFile;

class CatEstablecimientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv= new CsvFile(base_path().'/database/seeds/csv/cat_establecimiento.csv', ';');
        foreach($csv AS $row) {
          $estatus_operacion= new CatMaster;
          $estatus_operacion->create([
            'catalogo'=> 'cat_establecimiento',
            'etiqueta'=> $row[1],
            'cve'=> $row[0],
            'valor'=> $row[1],
          ]);
       }
    }
}
