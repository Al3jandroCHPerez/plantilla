<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        //
        DB::table('users')->insert(
          array(
            'id'=>1,
            'nombre'=>'Root',
            'puesto'=>'Administrador',
            'email'=>'root@issste.gob.mx',
            'email_verified_at'=>'2019-07-02 03:50:18',
            'password' => '$2y$10$.FZgGGzG6EdvTCAbVbknW.Yf.amT3dapMthFBJYaBA24gJzbOHfrm',
            'id_entidad'=>9,
            'subdireccion'=>'SCP',
            'telefono'=>'5513568979',
            'red'=>'35227',
            'id_unidad'=>1,
            'status'=>1,
            'remember_token'=>'',
            'created_at'=>'2019-07-02 03:50:18',
            'updated_at'=>'2019-07-02 03:50:18',
          )
        );
        DB::table('rol_user')->insert(
          array(
            'id'=>1,
            'id_user'=>1,
            'id_rol'=>1,
            'status'=>1,
          )
        );

    }
}
