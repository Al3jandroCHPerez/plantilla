<?php

use Illuminate\Database\Seeder;
use App\Models\CatMaster;
use Keboola\Csv\CsvFile;

class LoadNivelAtencionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $csv= new CsvFile(base_path().'/database/seeds/csv/nivelAtencion.csv', ',');
        foreach($csv AS $row) {
           $estrato_unidad= new CatMaster;
            $estrato_unidad->create([
            'catalogo'=>'cat_nivel_atencion', 
            'etiqueta'=>$row[1],
           	'cve'=> $row[0],
    		    'valor'=>$row[1],
       	    ]);
        }
    }
}
