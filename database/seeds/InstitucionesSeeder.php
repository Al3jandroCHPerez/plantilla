<?php

use Illuminate\Database\Seeder;
use Keboola\Csv\CsvFile;
use App\Models\CatMaster;

class InstitucionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $csv= new CsvFile(base_path().'/database/seeds/csv/cat_institucion.csv', ';');
        foreach($csv AS $row) {
    		  $institucion= new CatMaster;
           $institucion->create([
       		 'catalogo'=> 'cat_institucion',
           'etiqueta'=> $row[1],
           'cve'=> $row[2],
		       //	'etiqueta'=> $row[1],
           'valor'=> $row[0],
       	  ]);
        }
    }
}
