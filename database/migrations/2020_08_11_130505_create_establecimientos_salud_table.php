<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstablecimientosSaludTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establecimientos_salud', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('clues')->nullable();
            $table->integer('entidad_id')->nullable();
            $table->integer('municipio_id')->nullable();
            $table->integer('localidad_id')->nullable();
            $table->integer('jurisdiccion_id')->nullable();
            $table->string('institucion_id')->nullable();
            $table->integer('tipo_establecimiento_id')->nullable();
            $table->integer('tipologia_id')->nullable();
            $table->integer('consultoriosmed_gral')->nullable();
            $table->integer('consultoriosareas')->nullable();
            $table->integer('camas_hosp')->nullable();
            $table->integer('camas_areas')->nullable();
            $table->text('nombre_unidad')->nullable();
            $table->text('vialidad')->nullable();
            $table->string('numero_exterior')->nullable();
            $table->string('numero_interior')->nullable();
            $table->string('asentamiento')->nullable();
            $table->text('observaciones_de_la_direccion')->nullable();
            $table->string('codigo_postal')->nullable();
            $table->integer('status_id')->nullable();
            $table->string('rfc_del_establecimiento')->nullable();
            $table->integer('id_clave_presupuestal')->nullable();
            $table->integer('nivelatencion_id')->nullable();
            $table->integer('estratounidad_id')->nullable();
            $table->decimal('lat', 11, 8)->nullable();
            $table->decimal('lon', 11, 8)->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establecimientos_salud');
    }
}
