<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatJurisdiccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_jurisdiccion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jurisdiccion')->nullable();
            $table->integer('cve_jurisdiccion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_jurisdiccion');
    }
}
