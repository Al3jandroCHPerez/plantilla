<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClavePresupuestalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clave_presupuestal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('clues');
            $table->string('clave_presupuestal');
            $table->string('id_red')->nulleable();
            $table->integer('id_entidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clave_presupuestal');
    }
}
