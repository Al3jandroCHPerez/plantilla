<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_master', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('parent_id')->nullable()->default(null);
            $table->string('catalogo', 100)->nullable()->default(null);
            $table->string('etiqueta', 200)->nullable()->default(null); 
            $table->string('cve', 10)->nullable()->default(null);
            $table->integer('activo')->nullable()->default(null);
            $table->integer('orden')->nullable()->default(null);
            $table->string('valor')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_master');
    }
}
