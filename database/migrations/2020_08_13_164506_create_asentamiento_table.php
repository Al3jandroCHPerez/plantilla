<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsentamientoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asentamiento', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_tipo_asentamiento');
            $table->string('asentamiento');
            $table->integer('codigo_postal');
            $table->integer('cve_asentamiento');
            $table->string('tipo_zona');
            $table->integer('cve_ciudad');
            $table->integer('cve_municipio');
            $table->integer('id_entidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asentamiento');
    }
}
