// Varibles
var btnUserEdit = document.querySelectorAll('.btnUserEdit');
var btnUserChangePassword = document.querySelectorAll('.btnUserChangePassword');
var btnSendEditUser = document.querySelector('#btnSendEditUser');
const arrowTablePrevious = "<svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-arrow-left-short' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M7.854 4.646a.5.5 0 0 1 0 .708L5.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z'/><path fill-rule='evenodd' d='M4.5 8a.5.5 0 0 1 .5-.5h6.5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z'/></svg>";
const arrowTableNext = "<svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-arrow-right-short' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8.146 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.793 8 8.146 5.354a.5.5 0 0 1 0-.708z'/><path fill-rule='evenodd' d='M4 8a.5.5 0 0 1 .5-.5H11a.5.5 0 0 1 0 1H4.5A.5.5 0 0 1 4 8z'/></svg>";
const btnUpdatePassword = document.getElementById('btnUpdatePassword');
const responseChangePassword = document.getElementById('responseChangePassword');
const responseAddRol = document.getElementById('responseAddRol');
const btnAddRolUser = document.querySelectorAll('.btnAddRolUser');
const btnSendAddRolUser = document.querySelector('#btnSendAddRolUser');
const btnDeleteUser = document.querySelectorAll('.btnDeleteUser');
// iniciamos la creacion de la tabla
var userTable = $('#user_table').DataTable({
    deferRender: true, // ayuda en la carga de filas muy grandes
    select: true, // Si el usuario da click en la fila se pinta de color
    language: { // https://datatables.net/reference/option/language
        paginate: {
            previous: arrowTablePrevious, // establecemos el icono previous
            next: arrowTableNext // establecemos el icono next
        },
        info: "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        search: "Buscar:",
        lengthMenu: "Mostrar _MENU_ entradas",
        loadingRecords: "Cargando...",
        processing: "Procesando...",
        zeroRecords: "No se encontraron registros coincidentes.",
    }
});

// al dar clic en el paginador se manda a llamar eventlistener
// userTable.on( 'draw', function () {
//     addEventListenerTable();
// });

// Fin creacion de tabla


// eventListener
addEventListeners();
function addEventListeners(){
    for(i=0;i<btnUserEdit.length;i++){
        btnUserEdit[i].addEventListener('click', editUser);
    }
    for(i=0;i<btnUserChangePassword.length;i++){
        btnUserChangePassword[i].addEventListener('click', userChangePassword);
    }
    for(i=0;i<btnAddRolUser.length;i++){
        btnAddRolUser[i].addEventListener('click', addRolUser);
    }
    for(i=0;i<btnDeleteUser.length;i++){
        btnDeleteUser[i].addEventListener('click', deleteUser);
    }
    
    btnSendEditUser.addEventListener('click', sendEditUser);
    btnUpdatePassword.addEventListener('click', sendChangePassword);
    btnSendAddRolUser.addEventListener('click', sendAddRolUser)
}

function addEventListenerDeleteRol(){
    const btnDeleteRolUser = document.querySelectorAll('.btnDeleteRolUser');
    for(i=0;i<btnDeleteRolUser.length;i++){
        btnDeleteRolUser[i].addEventListener('click', deleteRol);
    }
}

// function addEventListenerTable(){
//     for(i=0;i<btnUserEdit.length;i++){
//         btnUserEdit[i].addEventListener('click', editUser);
//     }
//     for(i=0;i<btnUserChangePassword.length;i++){
//         btnUserChangePassword[i].addEventListener('click', userChangePassword);
//     }
// }


// funciones
function editUser(e){
    e.preventDefault();
    console.log(e.target.dataset.userid);
    getUser(e.target.dataset.userid)
}

function sendEditUser(e){
    // prevenimos el evento del boton
    e.preventDefault();
    // cargamos las varibles del input
    let inputs = getInputsEdit();
    // Ajax para actualizar
    fetch(`/updateData`, {
        method: 'POST',
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.stringify(inputs)
    })
    .then( response => response.json() ) // blob = binary large objet (Recibimos un objeto binario)
    .then( (response) => {
        if (response.status) {
            Swal.fire(
                'Actualización correcta',
                'El registro se actualizo correctamente. Es necesario recargar el sitio',
                'success'
            ).then((result) => {
                if (result.value) {
                    location.reload(); 
                }
              })
            
        }
    })
}

function deleteUser(e){
    e.preventDefault();
    Swal.fire({
        title: '¿Estas seguro?',
        text: "Al borrar el usuario se liminara por completo del sistema.",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
            fetch(`/api/deleteuser/${e.target.dataset.userid}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type':'application/json'
                }
            })
            .then( response => response.json() )
            .then( response => {
                e.target.parentNode.parentNode.parentNode.parentNode.remove()
                Swal.fire(
                    'Elimino!',
                    'El usuario se elimino de forma correcta.',
                    'success'
                )
            })
            
        }
      })
}

const getInputsEdit = () =>{
    let inputs = {
        _token: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
        id: document.querySelector('#user_id_edit').value,
        nombre: document.querySelector('#nombre_edit').value,
        puesto: document.querySelector('#puesto_edit').value,
        email: document.querySelector('#email_edit').value,
        id_entidad: document.querySelector('#id_entidad_edit').value,
        subdireccion: document.querySelector('#subdireccion_edit').value,
        telefono: document.querySelector('#telefono_edit').value,
        red: document.querySelector('#red_edit').value,
        id_unidad: document.querySelector('#id_unidad_edit').value,
    };
    return inputs;
}

const getUser = (id) => {
    fetch(`/api/user/${id}`)
    .then( response => response.json() ) // blob = binary large objet (Recibimos un objeto binario)
    .then( (response) => {
        document.getElementById('user_id_edit').value = response.data.id;
        document.getElementById('nombre_edit').value = response.data.nombre;
        document.getElementById('puesto_edit').value = response.data.puesto;
        document.getElementById('email_edit').value = response.data.email;
        document.getElementById('id_entidad_edit').value = response.data.entidad.id_entidad;
        document.getElementById('subdireccion_edit').value = response.data.subdireccion;
        document.getElementById('telefono_edit').value = response.data.telefono;
        document.getElementById('red_edit').value = response.data.red;
        document.getElementById('id_unidad_edit').value = response.data.unidad.id;
    })
}

// funcion para agregar roles a un usuario
function addRolUser(e){
    e.preventDefault();
    // creamos el loading para presentar informacion
    document.getElementById('tbodyRolUser').innerHTML = `
    <tr id="trLoading">
        <td colspan="3" style="text-align: center;">
            <div class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </td>
    </tr>
    `;
    // al campo oculto le agregamos el id
    document.getElementById('user_id_add_rol').value = e.target.dataset.userid;
    // ajax para solicitar la informacion de los roles
    fetch(`/api/getroluser/${e.target.dataset.userid}`)
    .then( response => response.json() )
    .then( (response) => {
        roles = response.data.roles_user;
        html = "";
        // recorremos el valor para crear la tabla
        roles.forEach((item, index) => {
            html += `
            <tr>
                <th scope="row">${index + 1}</th>
                <td>${item.roles.nombre_rol}</td>
                <td>
                    <button type="button" data-user="${response.data.id}" data-rol="${item.roles.id}" class="btn btn-danger btnDeleteRolUser flaticon2-trash"></button>
                </td>
            </tr>
            `;
        });
        // pintamos la tabla en la vista
        document.getElementById('tbodyRolUser').innerHTML= html;
        addEventListenerDeleteRol();
    })
}

function deleteRol(e){
    e.preventDefault();
    usuario = e.target.dataset.user;
    rol = e.target.dataset.rol;
    // console.log(usuario, rol);
    fetch(`/api/deleteroluser/${usuario}/${rol}`, {
        method: 'DELETE',
        headers: {
            'Content-Type':'application/json'
        }
    })
    .then( response => response.json() ) // blob = binary large objet (Recibimos un objeto binario)
    .then( response => {
        console.log(response.status)
        if (response.status == true) {
            e.target.parentNode.parentNode.remove();
            Swal.fire(
                'Actualización correcta',
                response.message,
                'success'
            );
        }
    })

}

function sendAddRolUser(e){
    e.preventDefault();
    let inputs = {
        _token: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
        id_rol: document.querySelector('#rol_add_user').value.trim(),
        id_user: document.querySelector('#user_id_add_rol').value,
    };
    if (inputs.id_rol=="") {
        messageAddRol("Selecciona el rol que quieres agregar.");
    }else{
        fetch(`/api/addUser`, {
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify(inputs)
        })
        .then( response => response.json() ) // blob = binary large objet (Recibimos un objeto binario)
        .then( (response) => {
            if (response.data.status==false) {
                messageAddRol(response.data.message);
            }else{
                Swal.fire(
                    'Actualización correcta',
                    response.data.message,
                    'success'
                ).then((result) => {
                    if (result.value) {
                        location.reload(); 
                    }
                  })
                $('#show_modal_add_rol').modal('hide');
                document.getElementById('rol_add_user').value="";
                document.getElementById('user_id_add_rol').value="";
            }
        })
    }
    
}
function messageAddRol(message, type="error"){
    let html;
    if (type == "error") {
        html = `
        <div class="alert alert-danger" role="alert">
            ${message}
        </div>
        `;
    } else {
        html = `
        <div class="alert alert-success" role="alert">
            ${message}
        </div>
        `;
    }
    responseAddRol.innerHTML = html;
    setTimeout(function(){
        responseAddRol.innerHTML = "";
    }, 3000);
}

// funcion para actualizar contraseña:
function userChangePassword(e) {
    e.preventDefault();
    document.getElementById('user_id_change_password').value=e.target.dataset.userid;
}

function sendChangePassword(e){
    e.preventDefault();
    let inputs = getInputsChangePassword();
    console.log(inputs);
    if (inputs.newPassword =="" || inputs.reNewPassword == "") {
        messageChangePassword("Los campos son obligatorios.");
    }else{
        if (inputs.newPassword == inputs.reNewPassword) {
            // Ajax para actualizar
            fetch(`/updateDataPassword`, {
                method: 'POST',
                headers: {
                    'Content-Type':'application/json'
                },
                body: JSON.stringify(inputs)
            })
            .then( response => response.json() ) // blob = binary large objet (Recibimos un objeto binario)
            .then( (response) => {
                if (response.status) {
                    Swal.fire(
                        'Actualización correcta',
                        'La contraseña se actualizo correctamente.',
                        'success'
                    )
                    $('#show_modal_password_edit').modal('hide');
                    document.getElementById('new_pass').value="";
                    document.getElementById('new_pass_confirm').value="";
                    document.getElementById('user_id_change_password').value="";
                }
            })
            // messageChangePassword("Listoooo.", "success");
        } else {
            messageChangePassword("Las contraseñas no son identicas.");
        }
    }
}

function messageChangePassword(message, type="error"){
    let html;
    if (type == "error") {
        html = `
        <div class="alert alert-danger" role="alert">
            ${message}
        </div>
        `;
    } else {
        html = `
        <div class="alert alert-success" role="alert">
            ${message}
        </div>
        `;
    }
    responseChangePassword.innerHTML = html;
    setTimeout(function(){
        responseChangePassword.innerHTML = "";
    }, 3000);
}

const getInputsChangePassword = () => {
    let inputs = {
        _token: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
        newPassword: document.querySelector('#new_pass').value.trim(),
        reNewPassword: document.querySelector('#new_pass_confirm').value.trim(),
        user_id: document.querySelector('#user_id_change_password').value,
    };
    return inputs;
}





/**
 * NOOOOOOO BOOOOORRAAAAAARRRRR
 * Dejo esta parte comentada para ir revisando
 * NOOOOOOO BOOOOORRAAAAAARRRRR
 */


    //   // iniciamos la creacion de la tabla
    //   var tabla1 = $('#inventario_table').DataTable({
    //       deferRender: true, // ayuda en la carga de filas muy grandes
    //       language: { // https://datatables.net/reference/option/language
    //           paginate: {
    //               previous: arrowTablePrevious, // establecemos el icono previous
    //               next: arrowTableNext // establecemos el icono next
    //           },
    //           info: "Mostrando _START_ a _END_ de _TOTAL_ entradas",
    //           search: "Buscar:",
    //           lengthMenu: "Mostrar _MENU_ entradas",
    //           loadingRecords: "Cargando...",
    //           processing: "Procesando...",
    //           zeroRecords: "No se encontraron registros coincidentes.",
    //       },
    //       ajax: {
    //           url: '/api/users', // hacemos la consulta
    //           dataSrc: '' //es un concepto grande hay que ir a esta url https://datatables.net/examples/ajax/
    //       },
    //       columns:[ // definimos las columnas que renderizara en la tabla
    //           { data: 'nombre' },
    //           { data: 'puesto' },
    //           { data: '' },
    //           { data: 'email' },
    //           { data: 'entidad.entidad'},
    //           { data: null, 
    //               render: function (data, type, row) {
    //                   var details = row.unidad.institucion.valor + " " + row.unidad.tipologia.etiqueta +" "+row.unidad.nombre_unidad;
    //                   return details;
    //               }
    //           },
    //           { data: 'id', 
    //             render: function (id) { 
    //               return `
    //               <div class="btn-group">
    //               <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    //                 <i class="fas fa-cog left"></i>
    //               </button>
    //               <div class="dropdown-menu">
    //                 <a class="dropdown-item btnUserEdit" data-userid="${id}" data-toggle="modal" data-target="#show_modal_edit" href="#"> <i class="fas fa-cog left"></i> Editar</a>
    //                 <div class="dropdown-divider" ></div>
    //                 <a class="dropdown-item btnUserChangePassword" data-userid="${id}" data-toggle="modal" data-target="#show_modal_password_edit" href="#"> <i class="fas fa-lock center"></i> Contraseña</a>
    //                 <div class="dropdown-divider" ></div>
    //                 <a class="dropdown-item btnAddRolUser" data-userid="${id}" data-toggle="modal" data-target="#show_modal_add_rol" href="#"> <i class="fas fa-plus center"></i> Rol</a>
    //                 <div class="dropdown-divider" ></div>
    //                 <a class="dropdown-item btnDeleteUser" data-userid="${id}" data-toggle="modal" href="#"> <i class="flaticon2-trash"></i>Eliminar</a>
    //               </div>
    //             </div>
    //               `; 
    //             } 
    //           }
              
    //       ]
    //   });

    //   tabla1.on('initComplete', function(){
    //     addEventListeners();
    //   })
      