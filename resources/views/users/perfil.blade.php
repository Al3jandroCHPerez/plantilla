@extends('layouts.layout')
@section('perfil')

<script>
$("#breadcrumb-title").append('Perfil  ');
</script>
<div class="row">
  <div class="col-xl-4 col-lg-4">
    <div class="m-portlet m-portlet--full-height  ">
      <div class="m-portlet__body">
        <div class="m-card-profile">
          <div class="m-card-profile__pic">
            <!-- imagen perfil -->
            <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_apps_user_add_avatar"  style="top: -90px;">
              <div class="profile-userpic" id="avatar_actual" >              
                <img src="{{ Storage::url($user[0]->avatar)}}" id="avatarImage">
              </div>
              <form action="{{ url('/subir_imagen') }}"  method="post"  id="avatarForm" >
              {{ csrf_field() }}
                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                <i class="flaticon-photo-camera"></i>
                <input type="file" id="avatarInput" name="avatar" style="display: none">
                </label>
              </form>
            </div>
            <!--fin imagen perfil -->
          </div>
          <div class="m-card-profile__details" style="text-align: center; line-height: 13px; font-weight: 600;">
            <div>
              <label>Nombre:</label>
              <span>{{($user[0]->nombre)}}</span>
            </div>
            <div>
              <label>Puesto:</label>
              <span>{{($user[0]->puesto)}}</span>
            </div>
            <div>
              <label>Email:</label>
              <span>{{($user[0]->email)}}</span>
            </div>
            <div>
              {{--<label>Rol:</label>
              <span>{{($user[0]->rol->nombre_rol)}}</span>--}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-8 col-lg-9">
    <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
      <div class="m-portlet__head">
        <div class="m-portlet__head-tools">
          <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
            <li class="nav-item m-tabs__item">
              <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                <i class="flaticon-share m--hide"></i>
                Cambio de Contraseña
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="tab-content">
        <div class="tab-pane active" id="m_user_profile_tab_1">
          <form class="m-form m-form--fit m-form--label-align-right" method="POST" id="editar_perfil">
             @csrf
            <div class="m-portlet__body">
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label" style="line-height: 15px;">
                    Contraseña Actual
                </label>
                <div class="col-7">
                  <input class="form-control m-input" autocomplete="cc-number" type="password" id="passwordd" name="passwordd" required>
                  <strong class="txt_passworda"></strong>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label" style="line-height: 15px;">
                    Nueva Contraseña
                </label>
                <div class="col-7">
                  <input class="form-control m-input" type="password" id="newpassword" name="password" >
                  <strong class="txt_passwordd"></strong>
                </div>
              </div>
              <div class="form-group m-form__group row">
                  <label for="example-text-input" autocomplete="cc-number" class="col-2 col-form-label" style="line-height: 15px;">
                    Confirmar Contraseña
                  </label>
                <div class="col-7">
                  <input class="form-control m-input" autocomplete="cc-number" type="password" id="newpasswordf" name="password" >
                  <strong class="txt_passwordd"></strong>
                </div>
              </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
              <div class="m-form__actions">
                <div class="row"> 
                  <div class="col-7">
                      <a id="guarda_perfil" class="btn btn-sm btn-label-primary btn-bold">
                        Guardar
                      </a>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection