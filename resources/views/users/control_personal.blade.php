@extends('layouts.layout')
@section('content')
<script>
$("#breadcrumb-title").append('Control de usuarios');
</script>
<div class="container-fluid m-portlet pt-5 pb-5">
  <header class="headerButton">
      <h2>Usuarios</h2>
      <a data-toggle="modal" data-target="#nuevo" id="reg_nue" href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
        <span><i class="fa fa-users left"></i><span>Nuevo Usuario</span></span>
      </a>
  </header>
  <hr>
  @if (\Session::has('success'))
      <div class="alert alert-success" role="alert">
          {!! \Session::get('success') !!}
      </div>
  @endif
  <div class="table-responsive mt-5">
    <table id="user_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>#</th>
          <th>Nombre</th>
          <th>Puesto</th>
          <th>Roles</th>
          <th>Email</th>
          <th>Delegación</th>
          <th>Unidad Laboral</th>   
          <th>Configuración</th>
        </tr>
      </thead>
      <tbody>
          @foreach ($usuarios as $user)
            <tr class="m-datatable__row m-datatable__row--even">
              <td class="m-datatable__cell">{{$user->id}}</td>
              <td class="m-datatable__cell">{{strtoupper($user->nombre)}}</td>
              <td class="m-datatable__cell">{{strtoupper($user->puesto)}}</td>
              <td class="m-datatable__cell">
                @foreach ($user->roles_user as $item)
                  <span class="badge badge-primary">{{strtoupper($item->roles->nombre_rol)}}</span>
                @endforeach
              </td>
              <td class="m-datatable__cell">{{$user->email}}</td>
              <td class="m-datatable__cell">{{strtoupper($user->entidad->entidad)}}</td>
              <td class="m-datatable__cell">{{$user->unidad->nombre_completo()}} </td>
              <td class="m-datatable__cell">
                <div class="btn-group">
                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-cog left"></i>
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item btnUserEdit" data-userid="{{$user->id}}" data-toggle="modal" data-target="#show_modal_edit" href="#"> <i class="fas fa-cog left"></i> Editar</a>
                    <div class="dropdown-divider" ></div>
                    <a class="dropdown-item btnUserChangePassword" data-userid="{{$user->id}}" data-toggle="modal" data-target="#show_modal_password_edit" href="#"> <i class="fas fa-lock center"></i> Contraseña</a>
                    <div class="dropdown-divider" ></div>
                    <a class="dropdown-item btnAddRolUser" data-userid="{{$user->id}}" data-toggle="modal" data-target="#show_modal_add_rol" href="#"> <i class="fas fa-plus center"></i> Rol</a>
                    <div class="dropdown-divider" ></div>
                    <a class="dropdown-item btnDeleteUser" data-userid="{{$user->id}}" data-toggle="modal" href="#"> <i class="flaticon2-trash"></i>Eliminar</a>
                  </div>
                </div>
              </td>
            </tr>
          @endforeach
      </tbody>
      <tfoot>
        <tr>
          <th>#</th>
          <th>Nombre</th>
          <th>Puesto</th>
          <th>Roles</th>
          <th>Email</th>
          <th>Delegación</th>
          <th>Unidad Laboral</th>   
          <th>Configuración</th>
        </tr>
    </tfoot>
    </table>
  </div>
  {{-- <div class="table-responsive mt-5">
    <table id="inventario_table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
      <thead>
          <tr>
              <th>Nombre</th>
              <th>Puesto</th>
              <th>Roles</th>
              <th>Email</th>
              <th>Delegación</th>
              <th>Unidad Laborar</th>
              <th>Configuración</th>
          </tr>
      </thead>
      <tfoot>
          <tr>
            <th>Nombre</th>
            <th>Puesto</th>
            <th>Roles</th>
            <th>Email</th>
            <th>Delegación</th>
            <th>Unidad Laborar</th>
            <th>Configuración</th>
          </tr>
      </tfoot>
  </table>
  </div> --}}
  @include('users/modales/nuevo_usuario')
  @include('users/modales/editar_usuario')
  @include('users/modales/editar_password')
  @include('users/modales/add_rol_user')
</div>
@endsection


@push('scripts')
    <script>
      $("#email").inputmask(
        {
          mask:"*{1,30}[.*{1,30}][.*{1,30}][.*{1,30}]@issste.gob.mx",
          greedy:!1,
          onBeforePaste:function(a,m){return(a=a.toLowerCase()).replace("mailto:","")},
          definitions:{
            "*":{
              validator:"[0-9A-Za-z]",
              cardinality:1,
              casing:"lower"
            }
          }
        }
      );
      $("#email_edit").inputmask(
        {
          mask:"*{1,30}[.*{1,30}][.*{1,30}][.*{1,30}]@issste.gob.mx",
          greedy:!1,
          onBeforePaste:function(a,m){return(a=a.toLowerCase()).replace("mailto:","")},
          definitions:{
            "*":{
              validator:"[0-9A-Za-z]",
              cardinality:1,
              casing:"lower"
            }
          }
        }
      );
      $("#telefono").inputmask(
        {
          mask:"9",
          repeat:10,
          greedy:!1,
        }
      );
      $("#red").inputmask(
        {
          mask:"9",
          repeat:10,
          greedy:!1,
        }
      );
    </script>
    <script src="{{ asset('js/functionsUsers.js') }}" defer></script>
@endpush
