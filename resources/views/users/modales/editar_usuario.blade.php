<div class="modal fade" id="show_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="nombreHeader">Editar Usuario</h5>
            <button type="button" id="cerrar_editar" class="flaticon2-delete btn btn-sm btn-label-primary btn-bold" style="font-size: 0.8rem;" data-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" id="modal_content">
            <form class="form-horizontal" role="form" id="edita_usuario">
              {{ csrf_field() }}
              <input type="hidden" name="user_id" id="user_id_edit">
       				<div class="modal-body">
                <div class="form-row">
         					<div class="form-group col-md-6" style="text-align: left;">
         						<label for="nombre" >Nombre (s)</label>
         						<input type="text" class="form-control" id="nombre_edit" name="nombre">
         					</div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="puesto" >Puesto</label>
                    <input type="text" class="form-control" id="puesto_edit" name="puesto">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="email" >Email</label>
                    <input type="text" class="form-control" id="email_edit" name="email">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="id_entidad" >Delegación</label>
                    <select id="id_entidad_edit" name="id_entidad" class="form-control">
                      <option value="" selected>Seleccione...</option>
                      @foreach ($catEntidad as $item)
                        <option value="{{$item->id_entidad}}">{{ucfirst($item->entidad)}}</option>
                      @endforeach
                    </select>
                    <strong class="text_estado"></strong>
                  </div>   
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="subdireccion" class="control-label">Subdirección</label>
                    <input type="text" class="form-control" id="subdireccion_edit" name="subdireccion">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="telefono" class="control-label">Teléfono</label>
                    <input type="text" class="form-control" id="telefono_edit" name="telefono">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="red" class="control-label">Red</label>
                    <input type="text" class="form-control" id="red_edit" name="red">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="id_unidad" class="control-label">Unidad Laboral</label>
                    <select class="form-control js-example-basic-single" id="id_unidad_edit" name="id_unidad">
                      <option value="" selected>Seleccione...</option>
                      @foreach ($establecimientoSalud as $item)
                        <option value="{{$item->id}}">{{ucfirst($item->nombre_completo())}}</option>
                      @endforeach
                    </select>
                      <strong class="text_unidad_laboral"></strong>
                  </div>  
       				  </div>
         				<div class="modal-footer">
                  <button type="button" id="btnSendEditUser" class="btn btn-sm btn-label-primary btn-bold" style="font-size: 1.4rem;">
         						<span class="fa fa-save"></span>
                     <span class="hidden-xs"> Guardar</span>
         					</button>
         				</div>
              </div>
       			</form>
          </div>
    </div>
  </div>
</div>
