<div class="modal fade" id="nuevo" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel" >Nuevo Usuario</h5>
                <button type="button" id="cerrar_modal"  data-dismiss="modal" aria-label="Close" class="flaticon2-delete btn btn-sm btn-label-primary btn-bold" style="font-size: 0.8rem;"></button>
            </div>
            <div class="modal-body" id="modal_content">
              
              <form class="form-horizontal" action="/registroUsuario" method="POST" role="form" id="nuevo_usuario">
          				<div class="modal-body">
                    <div class="form-row">
            					<div class="form-group col-md-6" style="text-align: left;">
            						<label  for="nombre">Nombre (s)</label>
            						<input type="text" class="form-control" id="nombre" name="nombre" required>
                        <strong class="text_nombre"></strong>
            					</div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="puesto"  >Puesto</label>
                        <input type="text" class="form-control" id="puesto" name="puesto" required>
                        <strong class="text_puesto"></strong>
                      </div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="email" class="control-label">Email</label>
                        <input type="text" class="form-control" id="email" name="email">
                          <strong class="text_email"></strong>
                      </div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="id_entidad">Delegación</label>
                        <select id="id_entidad" name="id_entidad" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true">
                          <option value="" selected>Seleccione...</option>
                          @foreach ($catEntidad as $item)
                            <option value="{{$item->id_entidad}}" data-tokens="{{$item->entidad}}">{{ucfirst($item->entidad)}}</option>
                          @endforeach
                        </select>
                        <strong class="text_estado"></strong>
                      </div>  
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="subdireccion" class="control-label">Subdirección</label>
                        <input type="text" class="form-control" id="subdireccion" name="subdireccion">
                          <strong class="text_subdireccion"></strong>
                      </div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="telefono" class="control-label">Teléfono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono">
                          <strong class="text_telefono"></strong>
                      </div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="red" class="control-label">Red</label>
                        <input type="text" class="form-control" id="red" name="red">
                          <strong class="text_red"></strong>
                      </div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="id_unidad" class="control-label">Unidad Laboral</label>
                        <select class="form-control m-bootstrap-select m_selectpicker" id="id_unidad" name="id_unidad" data-live-search="true">
                          <option value="" selected>Seleccione...</option>
                          @foreach ($establecimientoSalud as $item)
                            <option value="{{$item->id}}" data-tokens="{{$item->nombre_completo()}}">{{ucfirst($item->nombre_completo())}}</option>
                          @endforeach
                        </select>
                          <strong class="text_unidad_laboral"></strong>
                      </div>    
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="id_rol">Rol del usuario</label>
                          <select id="id_rol" name="id_rol" class="form-control">
                            <option value="" selected>Seleccione...</option>
                            @foreach ($rol as $item)
                              <option value="{{$item->id}}">{{ucfirst($item->nombre_rol)}}</option>
                            @endforeach
                          </select>
                          <strong class="text_rol"></strong>
                      </div> 
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="password" class="control-label">Contraseña</label>
                        <input type="password" autocomplete="cc-number" class="form-control" id="password" name="password" min="8">
                        <strong class="text_password"></strong>
                      </div>
                    </div>
                  </div>
            			<div class="modal-footer" >
            				<button type="submit" id="agregarUsuario" class="btn btn-sm btn-label-primary btn-bold">
                      <span class="fa fa-save"></span>
                      <span class="hidden-xs"> Guardar</span>
            				</button>   {{ csrf_field() }}
            			</div>
    			    </form>
            </div>
        </div>
    </div>
</div>
