<div class="modal fade" id="show_modal_add_rol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="false">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="nombreHeader">Añadir rol al usuario</h5>
              <button type="button" id="cerrar_editar" class="flaticon2-delete btn btn-sm btn-label-primary btn-bold" style="font-size: 0.8rem;" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="modal_content">
              <div id="responseAddRol"></div>
              <form class="form-horizontal" role="form">
                <input type="hidden" name="user_id" id="user_id_add_rol">
                <div class="modal-body">
                  <div class="form-row container">
                    <div class="form-group col-md-12" style="text-align: left;">
                        <label for="rol_add_user">Selecciona el rol:</label>
                        <select class="form-control" id="rol_add_user">
                            <option value="" selected>Seleccione...</option>
                            @foreach ($rol as $item)
                              <option value="{{$item->id}}">{{ucfirst($item->nombre_rol)}}</option>
                            @endforeach
                        </select>
                    </div>
                    
                  </div>
                  <div class="tableRolesUserEdit">
                    <h3>El Usuario cuanta con los siguientes roles</h3>
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Rol</th>
                          <th scope="col">Eliminar</th>
                        </tr>
                      </thead>
                      <tbody id="tbodyRolUser">
                      </tbody>
                    </table>
                  </div>
                  <div class="modal-footer">
                     <button type="button" id="btnSendAddRolUser" class="btn btn-sm btn-label-danger btn-bold btn-ok" style="font-size: 1.4rem;">
                        <span class="fa fa-save"></span>
                        <span class="hidden-xs">Agregar</span>
                     </button>
                    {{ csrf_field() }}
                  </div>
                </div>
              </form>
            </div>
      </div>
    </div>
</div>