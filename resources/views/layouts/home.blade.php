@extends('layouts.layout')

@section('content')
<div class="container-fluid">
    <header class="headerSection">
        <a href="#link" class="buttonReturn">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.354 10.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L6.207 7.5H11a.5.5 0 0 1 0 1H6.207l2.147 2.146z"/>
            </svg>
        </a>
        <h2>Título de la sección</h2>
    </header>
    <hr>
    <table id="inventario_table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>User ID</th>
                <th>Id</th>
                <th>Title</th>
                <th>Body</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>User ID</th>
                <th>Id</th>
                <th>Title</th>
                <th>Body</th>
            </tr>
        </tfoot>
    </table>
</div>
@endsection

@push('scripts')
    <script>
        // variables para colocar iconos svg
        const arrowTablePrevious = "<svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-arrow-left-short' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M7.854 4.646a.5.5 0 0 1 0 .708L5.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z'/><path fill-rule='evenodd' d='M4.5 8a.5.5 0 0 1 .5-.5h6.5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z'/></svg>";
        const arrowTableNext = "<svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-arrow-right-short' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8.146 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.793 8 8.146 5.354a.5.5 0 0 1 0-.708z'/><path fill-rule='evenodd' d='M4 8a.5.5 0 0 1 .5-.5H11a.5.5 0 0 1 0 1H4.5A.5.5 0 0 1 4 8z'/></svg>";
        // iniciamos la creacion de la tabla
        $('#inventario_table').DataTable({
            deferRender: true, // ayuda en la carga de filas muy grandes
            scrollY: 300, // Establece un alto maximo para colocar un scroll
            scrollX: true, // aceptamos el uso de scroll en eje x
            select: true, // Si el usuario da click en la fila se pinta de color
            language: { // https://datatables.net/reference/option/language
                paginate: {
                    previous: arrowTablePrevious, // establecemos el icono previous
                    next: arrowTableNext // establecemos el icono next
                },
                info: "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                search: "Buscar:",
                lengthMenu: "Mostrar _MENU_ entradas",
                loadingRecords: "Cargando...",
                processing: "Procesando...",
                zeroRecords: "No se encontraron registros coincidentes.",
            },
            ajax: {
                url: 'https://jsonplaceholder.typicode.com/posts', // hacemos la consulta
                dataSrc: '' //es un concepto grande hay que ir a esta url https://datatables.net/examples/ajax/
            },
            columns:[ // definimos las columnas que renderizara en la tabla
                { data: 'userId' },
                { data: 'id' },
                { data: 'title' },
                { data: 'body' }
            ]
        });
    </script>
    <script src="{{ asset('js/app.js') }}" defer></script>
@endpush