@if ($item['submenu'] == [])
    <li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
      <a href="{{$item['url']}}" class="kt-menu__link">
        <i class="{{$item['clase']}} "><span></span></i>
        <span class="kt-menu__link-text">{{$item['etiqueta']}}</span>
      </a>
    </li>
  @else
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
      <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-user-ok"><span></span></i>
         <span class="kt-menu__link-text">{{$item['etiqueta']}}</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
      </a>
      @foreach ($item['submenu'] as $submenua)
        @if ($submenua['submenu'] == [])
          <div class="kt-menu__submenu ">
            <span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
              <li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">

                <a class="kt-menu__link " href="{{$submenua['url']}}">

                  <i class="{{$submenua['clase']}}"><span></span></i>
                  <span class="kt-menu__link-text">{{$submenua['etiqueta']}}</span>
                </a>
              </li>
            </ul>
          </div>
          @else
            @include('layouts.sub_menus', [ 'item' => $submenu ])
        @endif
      @endforeach
@endif
