<!DOCTYPE html>
<html lang="es">
<head>
    <base href="../">
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=env('APP_NAME')?></title>
    @include('plantilla/script')
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-body kt-aside--minimize">
    <div id="initpreloader">
        <div id="initloader">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div>
    </div>
    <div class="kt-page-loader kt-page-loader--base kt-page-loader--non-block" style="margin-left: -80px; margin-top: -20px;">
        <div class="kt-blockui">
            <span>
                Cargando ...
            </span>
            <span>
                <div class="kt-loader m-loader--brand"></div>
            </span>
        </div>
    </div>

    <div id="kt_header_mobile" class="kt-header-mobile kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a>
                <img alt="Logo" src="{{asset('/assets/media/logos/ISSSTE_logo.png')}}"/>
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler" href="javascript:;"  ><span></span></button>
        </div>
    </div>
    
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <!--[html-partial:include:{"file":"partials\/_aside-base.html"}]/-->
            {{-- @include('plantilla/aside-base') --}}
            @include('plantilla/menuSite')
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                <!--[html-partial:include:{"file":"partials\/_header-base.html"}]/-->
                @include('plantilla/header-base')
                <!--[html-partial:include:{"file":"partials\/_subheader-subheader-v1.html"}]/-->
                <!-- begin:: Content -->
                <!--[html-partial:include:{"file":"partials\/_content-base.html"}]/-->
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                    <!-- begin:: Content -->
                    <!-- <div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
                        <div >
                            @yield('content')
                            @yield('content_perfil')
                        </div>                            
                    </div> -->
                    @include('plantilla/content-base')
                    <!-- end:: Content -->
                </div>
                <!--[html-partial:include:{"file":"partials\/_footer-base.html"}]/-->
                @include('plantilla/footer-base')
            </div>
        </div>
    </div>

    {{-- scripts iniciales para todo el sitio --}}
    @include('plantilla.script-global')
    {{-- Scripts por seccion --}}
    @stack('scripts')
</body>
</html>