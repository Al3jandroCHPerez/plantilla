<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><?=env('APP_NAME')?></title>
  <meta name="description" content="Latest updates and statistic charts">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!--begin::Web font -->
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
  <script>
        WebFont.load({
          google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
          active: function() {
              sessionStorage.fonts = true;
          }
        });
  </script>
  <!--end::Web font -->
      <!--begin::Base Styles -->
  <link href="css/style.css"/>
  <link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
  <link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
  <!--end::Base Styles -->
  <link rel="shortcut icon" href="assets/demo/default/media/img/logo/ISSSTE_logo.png" />
</head>
  <body>
    <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--singin" id="m_login">
                <div  class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
                    <div class="m-stack m-stack--hor m-stack--desktop">
                        <div class="m-stack__item m-stack__item--fluid">
                            <div class="m-login__wrapper">
                                <div class="m-login__logo">Gases Medicinales</div>
                  <!-- FORMULARIO DE INICIO DE SESSION -->
                                <div class="m-login__signin">
                                    <div class="m-login__head">
                                        <h3 class="m-login__title">Ingrese sus Credenciales</h3>
                                    </div>

                                    <form method="POST" action="{{ route('login') }}">
                                                 {{ csrf_field() }}
                          <!-- ENTRY DEL EMAIL -->
                      <div class="form-group m-form__group">
                        <input id="email" type="email"
                        class="form-control @error('email') is-invalid @enderror"
                        name="email"
                        value="{{ old('email') }}"
                        required autocomplete="email" autofocus placeholder="Correo Electrónico" >
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                         <!-- ENTRY DEL PASSWORD -->
                                        <div class="form-group m-form__group">
                        <input id="password" type="password"
                        class="form-control @error('password') is-invalid @enderror"
                        name="password"
                        required autocomplete="current-password" placeholder="Contraseña" >
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                           <!-- CHECKBOX RECORDARME -->
                                        <div class="row m-login__form-sub">
                                            <div class="col m--align-left">
                          <div class="form-check">
                              <input class="form-check-input"
                              type="checkbox" name="remember"
                              id="remember" {{ old('remember') ? 'checked' : '' }}>
                              <label class="form-check-label" for="remember">
                                  {{ __('Recordarme') }}
                              </label>
                          </div>
                                            </div>
                                        </div>
                                        <!-- BOTON -->
                                        <div class="m-login__form-action">
                                            <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air loginfnxx" onclick="validateUser()">{{ __('Ingresar') }}</button>
                                        </div>
                                        <!-- ENLACE PARA CONTRASEÃ‘A OLVIDADA -->
                                    </form>
                                </div>
                            </div>
                        </div>
                  <!-- SPAN DERECHOS DE AUTOS -->
                        <div class="m-stack__item m-stack__item--center">
                            <div class="m-login__account">
                                <span class="m-login__account-msg">
                                    2020 &copy; <?=env('APP_NAME')?>
                                </span>
                                <span class="m-login__account-msg">
                    <a href="">ISSSTE</a>
                                </span>
                                &nbsp;&nbsp;
                                <span href="javascript:;"
                    id="[dis]m_login_signup"
                    class="m-link m-link--focus m-login__account-link">
                                </span>
                            </div>
                        </div>

                    </div>
                </div>
                <div style="background-image: url(assets/app/media/img/bg/bg-home.png);width: 100%;background-size: cover;background-position: center;">
                </div>
            </div>
        </div>
        <!-- end:: Page -->
        <!--begin::Base Scripts -->
        <script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
        <script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Base Scripts -->
          <!--begin::Page Snippets -->
        <script src="assets/snippets/pages/user/login.js" type="text/javascript"></script>
        <!--end::Page Snippets -->
      <script>var app_url = '<?=env('APP_URL')?>';</script>
      <script src="assets/js/generales.js"></script>
      <script src="assets/js/common.js"></script>
  </body>
</html>