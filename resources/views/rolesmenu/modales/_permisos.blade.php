<div class="modal fade" id="{{'permisos'.$rol}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Permisos Rol: {{$nombre}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{url('/permisos')}}" method="POST">
        @csrf 
        <input type="hidden" name="id_rol" value="{{$rol}}"> 
        <div class="modal-body">
           @foreach($menu as $recorre)
            <div class="form-row">
              <div class="form-group col-md-5" style="text-align: left;">
                <label for="etiqueta" class="control-label">{{$recorre->etiqueta}}</label>      
              </div>
              <div class="form-group col-md-2">
              </div>
              <div class="form-group col-md-5">
                <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--danger">
                  <label><input type="checkbox"  name="id_menu[]" value="{{$recorre->id}}"><span></span></label>
                </span>
              </div>
            </div>
          @endforeach
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </form>
    </div>
  </div>
</div>