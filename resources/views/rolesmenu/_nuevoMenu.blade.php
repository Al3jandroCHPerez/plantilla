
<div class="col-md-6">
	<div class="card card-custom gutter-b example example-compact">
		<div class="card-header">
			<h3 class="card-title">Agregar Menú</h3>
		</div>
		<form action="{{url('/nuevomenu')}}" method="POST">
			@csrf 
			<div class="card-body">
				<div class="form-group">
					<label for="etiqueta">Etiqueta <span class="text-danger">*</span></label>
					<input  id="etiqueta" name="etiqueta" class="form-control" placeholder="Etiqueta" />
				</div>
				<div class="form-group">
					<label for="url">URL<span class="text-danger">*</span></label>
					<input class="form-control" id="url" name="url" placeholder="URL" />
				</div>	
				<div class="form-group">
					<h5>Para seleccionar icono consultar el siguiente<a href="https://preview.keenthemes.com/metronic/demo1/features/icons/flaticon.html" target="_blank"> <span class="flaticon-paper-plane-1 "></span>Link</a></h5>
					<label for="icono">Icono<span class="text-danger">*</span></label>
					<input class="form-control" id="icono" name="icono" placeholder="Icono" />
				</div>				
			</div>
			<div class="card-footer">
				<button type="submit" class="btn btn-primary mr-2">Guardar</button>
			</div>
		</form>
	</div>
</div>