<div class="col-md-6">
	<div class="card card-custom gutter-b example example-compact">
		<div class="card-header">
			<h3 class="card-title">Lista de Roles</h3>
		</div>
		<div class="card-body">
			<div class="form-group">
				<table id="roles_table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>Rol</th>
			                <th>Nombre Rol</th>
			                <th>Descripción</th>
			                <th>Elimina</th>
			                <th>Más</th>
			            </tr>
			            <tbody>
			            	@foreach($lista as $recorre)
			            		<tr>
			            			<td>{{$recorre->rol}}</td>
			            			<td>{{$recorre->nombre_rol}}</td>
			            			<td>{{$recorre->descripcion}}</td>
			            			<td>
			            				<form action="{{url('/eliminarol')}}" method="post" enctype="multipart/form-data">
           									@csrf 
           									<input type="hidden" name="id" value="{{$recorre->id}}"> 
								            <button class="btn btn-danger btn-xs" type="submit"></button>
								        </form> 
			            			</td>
			            			<td>
			            				<button type="button" class="btn btn-success" data-toggle="modal" data-target="{{'#permisos'.$recorre->id}}" ></button>
			            				@include('rolesmenu.modales._permisos',['menu' => $menu, 'nombre'=>$recorre->nombre_rol, 'rol' => $recorre->id ])
			            			</td>
			            		</tr>
			            	@endforeach
			            </tbody>
			        </thead>
			    </table>
			</div>					
		</div>
	</div>
</div>
@push('scripts')
<script type="text/javascript">
	const arrowTablePrevious = "<svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-arrow-left-short' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M7.854 4.646a.5.5 0 0 1 0 .708L5.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z'/><path fill-rule='evenodd' d='M4.5 8a.5.5 0 0 1 .5-.5h6.5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z'/></svg>";
    const arrowTableNext = "<svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-arrow-right-short' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8.146 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.793 8 8.146 5.354a.5.5 0 0 1 0-.708z'/><path fill-rule='evenodd' d='M4 8a.5.5 0 0 1 .5-.5H11a.5.5 0 0 1 0 1H4.5A.5.5 0 0 1 4 8z'/></svg>";
	$('#roles_table').DataTable({
            scrollY: 135, // Establece un alto maximo para colocar un scroll
            scrollX: true, // aceptamos el uso de scroll en eje x
            select: true, // Si el usuario da click en la fila se pinta de color
            language: { // https://datatables.net/reference/option/language
                paginate: {
                    previous: arrowTablePrevious, // establecemos el icono previous
                    next: arrowTableNext // establecemos el icono next
                },
                info: "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                search: "Buscar:",
                lengthMenu: "Mostrar _MENU_ entradas",
                loadingRecords: "Cargando...",
                processing: "Procesando...",
                zeroRecords: "No se encontraron registros coincidentes.",
            }
        });
</script>
@endpush