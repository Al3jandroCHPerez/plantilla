
<div class="col-md-6">
	<div class="card card-custom gutter-b example example-compact">
		<div class="card-header">
			<h3 class="card-title">Agregar Rol</h3>
		</div>
		<form action="{{url('/nuevorol')}}" method="POST" >
			@csrf 
			<div class="card-body">
				<div class="form-group">
					<label or="rol">Rol<span class="text-danger">*</span></label>
					<input type="rol" class="form-control" id="rol" name="rol" placeholder="Rol" />
				</div>
				<div class="form-group">
					<label for="nombre_rol">Nombre Rol<span class="text-danger">*</span></label>
					<input type="nombre_rol" class="form-control" id="nombre_rol" name="nombre_rol" placeholder="Nombre Rol" />
				</div>	
				<div class="form-group">
					<label for="descripcion_rol">Descripción del Rol<span class="text-danger">*</span></label>
					<input type="descripcion_rol" class="form-control" id="descripcion_rol" name="descripcion_rol" placeholder="Descripción del Rol" />
				</div>					
			</div>
			<div class="card-footer">
				<button  type="submit" class="btn btn-primary mr-2">Guardar</button>
			</div>
		</form>
	</div>
</div>