@extends('layouts.layout')

@section('content')
<script>
$("#breadcrumb-title").append('Control de Menú');
</script>

<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="row">

			@include('rolesmenu._nuevoMenu')
			@include('rolesmenu._listaMenu')
		
		</div>
	</div>
</div>

@endsection