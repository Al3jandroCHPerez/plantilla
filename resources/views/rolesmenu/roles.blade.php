@extends('layouts.layout')

@section('content')
<script>
$("#breadcrumb-title").append('Control de Roles');
</script>
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="row">

			@include('rolesmenu._nuevoRol')
			@include('rolesmenu._listaRoles')
		
		</div>
	</div>
</div>


@endsection