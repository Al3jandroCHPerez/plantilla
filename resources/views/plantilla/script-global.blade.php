<!--Inicio::Base Scripts -->
<script src="{{asset('assets/vendors/base/vendors.bundle.js')}}" />
<!--<script src="{{asset('assets/demo/default/base/scripts.bundle.js')}}" />-->
<script src="{{asset('assets/js/demo12/pages/dashboard.js')}}" />
<script src="{{asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" />

<!--Internacionalizacion para calendario datepicker -->
<script src="assets/vendors/base/spanish_ui.js" type="text/javascript"></script>
<!--Inicio::Base Scripts -->
<script src="{{asset('assets/vendors/perfect-scrollbar/dist/perfect-scrollbar.js')}}" />
<!--Slim Scroll-->
<script src="{{asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.js')}}" />

<script src="{{asset('assets/plugins/bootstrap-sessiontimeout/bootstrap-session-timeout.js')}}" />
<script src="{{asset('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.min.js')}}" />
<script src="{{asset('assets/plugins/buzz/buzz.min.js')}}" />
<script src="{{asset('assets/plugins/bootstrap-toastr/toastr.min.js')}}" />
// <!--JS-Sweet Alert-->
<script src="{{asset('assets/plugins/bootstrap-sweetalert/sweetalert.min.js')}}" />
// <!--JS-Dropzone-->
<script src="{{asset('assets/js/dropzone.js')}}"></script>


<!--Datatables-->
<script src="{{asset('assets/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>

<!--Autocomplete-->
<script src="{{asset('assets/js/jquery.autocomplete.js')}}" ></script>
<script src="{{asset('https://js.pusher.com/4.1/pusher.min.js')}}"></script>

// <!--Select Boostrap-->
<script src="{{asset('assets/js/bootstrap-select.js')}}"></script>

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{{asset('./assets/js/demo12/scripts.bundle.js')}}" />
<!--end::Global Theme Bundle -->
// Js-MAPA
<!--begin::Page Vendors(used by this page) -->
<script src="{{asset('//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM')}}" />
<script src="{{asset('./assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" />
<script src="{{asset('./assets/vendors/custom/gmaps/gmaps.js')}}" />

<!--end::Page Scripts -->

<!--select2 -->
<script src="{{asset('assets/js/select2.min.js')}}" />

// <!-- // Js-Sistema-Censo -->
<script src="{{asset('assets/js/controllers.js')}}"></script>
<script src="{{asset('assets/js/generales.js')}}"></script>
<script src="{{asset('assets/js/common.js')}}"></script>
// <!-- <script src="{{asset('assets/js/catalogo.js')}}"></script> -->
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<script src="{{asset('assets/js/estadisticas.js')}}"></script>
<script src="{{asset('assets/js/detallesunidades.js')}}"></script>
<!-- // Js para Usuarios -->
<script src="{{asset('assets/js/abc_usuario.js')}}"></script>
<script src="{{asset('assets/js/mi_perfil.js')}}"></script>
<!-- Optional: include a polyfill for ES6 Promises for IE11 -->


<!-- JS-PARA PROTECCIONPERSONAL -->

