<!-- begin:: Aside -->
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside kt-aside--fixed kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
	<!-- begin:: Aside -->
	<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
		<div class="kt-aside__brand-logo">
			<a href="/home">
				<img alt="Logo" src="{{asset('/assets/media/logos/ISSSTE_logo.png')}}">
			</a>
		</div>
		<div class="kt-aside__brand-tools">
			<button href="javascript:;" class="kt-aside__brand-aside-toggler" id="kt_aside_toggler"><span></span></button>
		</div>
	</div>
	<!-- end:: Aside -->
	<!-- begin:: Aside Menu -->
	<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
		<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
			<ul class="kt-menu__nav ">
				<li class="kt-menu__section kt-menu__section--first " aria-haspopup="true">
					<h4 class="kt-menu__section-text">
						<?=env('APP_NAME')?>
					</h4>
					<i class="kt-menu__section-icon flaticon-more-v2"></i>
                </li>
                <li class="kt-menu__item" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
					<a href="/home" class="kt-menu__link kt-menu__toggle">
						<i class="kt-menu__link-icon flaticon-home-1"></i>
                        <span class="kt-menu__link-text">Inicio</span>
                    </a>
                </li>
                @foreach (Auth::user()->menuPrincipal2() as $item)
                        @if (Auth::user()->validaRolMenuUsuario($item) == 1)
                            @if (!$item->submenu)
                                <li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                    <a href="{{$item['url']}}" class="kt-menu__link">
                                    <i class="{{$item['clase']}} "><span></span></i>
                                    <span class="kt-menu__link-text">{{$item['etiqueta']}}</span>
                                    </a>
                                </li>	
                            @else
                                <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-user-ok"><span></span></i>
                                        <span class="kt-menu__link-text">{{$item['etiqueta']}}</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
                                    </a>
                                    @foreach ($item->submenu as $item)
                                    <div class="kt-menu__submenu ">
                                        <span class="kt-menu__arrow"></span>
                                        <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                            <a class="kt-menu__link " href="{{$item->url}}">
                                            <i class="{{$item->clase}}"><span></span></i>
                                            <span class="kt-menu__link-text">{{$item->etiqueta}}</span>
                                            </a>
                                        </li>
                                        </ul>
                                    </div>	
                                    @endforeach
                                </li>
                            @endif	
                        @endif
                @endforeach
			</ul>
		</div>
	</div>
	<!-- end:: Aside Menu -->
</div>
<!-- end:: Aside -->