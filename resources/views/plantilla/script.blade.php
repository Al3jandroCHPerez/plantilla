<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
<link href="{{asset('assets/css/demo12/style.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />

<link href="{{asset('assets/css/demo12/pages/general/wizard/wizard-4.css')}}" rel="stylesheet" type="text/css" />

<!--Calendario-->
{{-- comente para mejorar LUIS --}}
{{-- <link href="{{asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" /> --}}

<!--JQuery-->
<script src="{{asset('assets/plugins/jquery-3.2.1.min.js')}}"></script>

<!--Plugins JQuery-->
<link href="{{asset('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />

<!--APP-->
<link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">

<!--calendar-->
<link href="{{asset('assets/vendors/custom/jquery-ui/jquery-ui.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />

<!--iconos-->
<!-- <link href="css/svg.css')}}" rel="stylesheet" type="text/css"> -->
<link href="{{asset('assets/vendors/custom/vendors/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/custom/vendors/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css" />

<!--Loader-->
<link href="{{asset('assets/css/loader.css')}}" rel="stylesheet" type="text/css" />
<script src="{{asset('assets/js/loader.js')}}"></script>

<!--perfect-scrollbar-->
<link href="{{asset('assets/vendors/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" type="text/css" />

<!--fontawesome5-->
<link href="{{asset('assets/vendors/vendors/fontawesome5/css/all.min.css')}}" rel="stylesheet" type="text/css" />

<!--Datatables-->
<link href="{{asset('assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" href="{{asset('assets/demo/default/media/img/logo/ISSSTE_logo.png')}}" />

<!--SELECT-->
<link href="{{ asset('assets/css/select2.min.css') }} " rel="stylesheet" type="text/css" />